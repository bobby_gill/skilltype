import memberships from './data/memberships.json'
import strategicDirections from './data/strategicDirections.json'
import products from './data/products.json'
import { prefixTagData, dedupe } from './utils'

const suggestionsByType = {
  memberships: dedupe(memberships.map(t => prefixTagData(t, 'membership'))),
  strategicDirections: dedupe(
    strategicDirections.map(t => prefixTagData(t, 'strategicDirection'))
  ),
  products: dedupe(products.map(t => prefixTagData(t, 'product'))),
}

const getTagGroups = (organizationData, organizationMeta) => ({
  memberships: {
    ...organizationMeta.memberships,
    canEdit: true,
    tags: organizationData.memberships.map(t => prefixTagData(t, 'membership')),
    category: organizationData.categories.filter(
      c => c.uniqueName === 'memberships'
    )[0],
    tagLinkable: false,
    suggestionsType: 'memberships',
  },
  strategicDirections: {
    ...organizationMeta.strategicDirections,
    canEdit: true,
    tags: organizationData.strategicDirections.map(t =>
      prefixTagData(t, 'strategicDirection')
    ),
    category: organizationData.categories.filter(
      c => c.uniqueName === 'strategic-directions'
    )[0],
    tagLinkable: false,
    suggestionsType: 'strategicDirections',
  },
  productsAndServices: {
    ...organizationMeta.productsAndServices,
    canEdit: true,
    tags: organizationData.productsAndServices.map(t =>
      prefixTagData(t, 'product')
    ),
    category: organizationData.categories
      .filter(c => c.uniqueName === 'products')
      .map(c => ({
        ...c,
        tags: c.tags.map(t => ({ ...t, href: `/tag/${t.id}` })),
      }))[0],
    tagLinkable: true,
    suggestionsType: 'products',
  },
})

export default {
  suggestionsByType,
  getTagGroups,
}
