/* eslint-disable
  jsx-a11y/click-events-have-key-events,
  jsx-a11y/interactive-supports-focus ,
  jsx-a11y/mouse-events-have-key-events
*/
import React from 'react'
import classnames from 'classnames'
import injectSheet from 'react-jss'
import scrollIntoView from 'scroll-into-view-if-needed'
import styles from './styles'
import CheckSvg from '../../assets/check.svg'
import { getAriaProps } from '../../lib/props'
import CloseButton from '../Button/CloseButton'
import SearchSvg from '../../assets/search.svg'
import Progress from '../Progress/Progress'
import ArrowIcon from '../../assets/arrow.svg'
import TransparentButton from '../Button/TransparentButton'
import SubmitButton from '../Button/SubmitButton'

class ListPicker extends React.Component {
  constructor(props) {
    super(props)
    this.inputRef = React.createRef()
    this.statusRef = React.createRef()
    this.filterStatusRef = React.createRef()
    this.listboxRef = React.createRef()

    // we start in keyboard mode and set this to false on mousemove
    // this prevents arbitrary mouse hovering from interfering with keyboard
    //   navigation
    this.keyboardMode = true
  }
  state = {
    suggestion: '',
    isSuggestionView: false,
  }
  componentDidUpdate(prevProps) {
    const { suggestions } = this.props
    if (suggestions.length !== prevProps.suggestions.length) {
      if (this.statusRef.current) {
        this.statusRef.current.textContent = ''
      }
      if (this.filterStatusRef.current) {
        this.filterStatusRef.current.textContent = this.getFilterStatus()
      }
    }
  }
  onQueryChange = e => {
    if (this.listboxRef.current) {
      this.listboxRef.current.scrollTo(0, 0)
    }

    if (this.props.onQueryChange) {
      this.props.onQueryChange(e.target.value)
    }
  }
  onActiveIndexChange = index => {
    if (this.props.onActiveIndexChange) {
      this.props.onActiveIndexChange(index)
    }
  }
  onListboxFocus = () => this.onFocus()
  onInputKeyDown = e => {
    if (this.props.disabled) {
      return
    }
    // switch back to keyboard mode until the next mousemove
    this.keyboardMode = true
    const { suggestions, activeIndex, onItemSelected, selected } = this.props
    switch (e.keyCode) {
      case 40: {
        // Down arrow
        this.advanceActiveOption(1)
        e.preventDefault()
        break
      }
      case 38: {
        // Up arrow
        this.advanceActiveOption(-1)
        e.preventDefault()
        break
      }
      case 32: {
        // spacebar
        if (activeIndex >= 0) {
          if (onItemSelected) {
            const id = suggestions[activeIndex].id
            this.statusRef.current.textContent = `${
              selected[id] ? 'You unselected:' : 'You selected: '
            } ${suggestions[activeIndex].fullName}`
            onItemSelected(suggestions[activeIndex].id, activeIndex)
          }
          e.preventDefault()
        }
        break
      }
      case 27: {
        // escape
        if (this.props.onClearInput) {
          this.props.onClearInput()
        }
        e.preventDefault()
        break
      }
      default: {
        break
      }
    }
  }
  onListboxKeyDown = e => {
    if (this.props.isCombo) {
      return
    }
    this.onInputKeyDown(e)
  }
  onMouseMove = () => {
    this.keyboardMode = false
  }
  onInputRef = e => {
    this.inputRef.current = e
    if (this.props.inputRef) {
      this.props.inputRef.current = e
    }
  }
  onListboxRef = e => {
    this.listboxRef.current = e
    if (this.props.listboxRef) {
      this.props.listboxRef.current = e
    }
  }
  onSuggestionChange = ({ target }) => {
    this.setState({
      suggestion: target.value,
    })
  }
  onSuggestTag = e => {
    const { suggestion } = this.state
    const { suggestTag } = this.props
    e.preventDefault()

    if (suggestion.length > 0 && suggestion.length <= 50) {
      suggestTag(suggestion, () => {
        this.setState({
          suggestion: '',
        })
      })
    }
  }
  getFilterStatus = () => {
    const length = this.props.suggestionsByType
    const items = length === 1 ? 'item' : 'items'
    return `${length} ${items}`
  }
  makeOnOptionClick = (id, idx) => () => {
    if (this.props.disabled) {
      return
    }
    const { onItemSelected } = this.props
    if (onItemSelected) {
      onItemSelected(id, idx)
    }
    if (this.props.isAlwaysCombo) {
      this.inputRef.current.focus()
    }
  }
  makeOnOptionMouseOver = idx => () => {
    if (this.props.disabled) {
      return
    }
    if (this.keyboardMode) {
      return
    }
    this.onActiveIndexChange(idx)
  }
  makeOptionRef = idx => e => {
    if (e && this.keyboardMode && idx === this.props.activeIndex) {
      scrollIntoView(e, { scrollMode: 'if-needed' })
    }
  }
  advanceActiveOption = incrementBy => {
    const { suggestions, activeIndex } = this.props
    let nextIndex = activeIndex + incrementBy
    if (nextIndex === suggestions.length) {
      nextIndex = 0
    } else if (nextIndex < 0) {
      nextIndex = suggestions.length - 1
    }
    this.onActiveIndexChange(nextIndex)
  }
  switchViewMode = () => {
    const { isSuggestionView } = this.state
    const elem = document.querySelector('.ReactModalPortal footer')

    if (isSuggestionView) {
      this.setState({
        isSuggestionView: false,
      })
      elem.style.display = 'flex'
    } else {
      this.setState({
        isSuggestionView: true,
        suggestion: '',
      })
      elem.style.display = 'none'
    }
  }
  renderContent = () => {
    const { isSuggestionView, suggestion } = this.state
    const {
      classes,
      className,
      style,
      query,
      suggestions,
      placeholder,
      ariaLabel,
      isMultiSelect,
      activeIndex,
      id,
      selected,
      disabled,
      selectedColor,
      onInputFocus,
      onClearInput,
      isCombo,
      isAlwaysCombo,
      listboxRef,
      noResultsMessage,
      addMobileKeyboardPadding,
      theme,
      ListFallback,
      ...others
    } = this.props

    if (isSuggestionView) {
      return (
        <form
          className={classes.suggestionContent}
          onSubmit={this.onSuggestTag}
        >
          <div className={classes.suggestionHeader}>
            <TransparentButton onClick={this.switchViewMode}>
              <ArrowIcon />
            </TransparentButton>
            <span>Type New Tag Suggestion</span>
            <span className={classes.suggestionLength}>
              {suggestion.length}/50
            </span>
          </div>
          <input
            type="text"
            className={classes.suggestionInput}
            value={suggestion}
            onChange={this.onSuggestionChange}
            maxLength={50}
          />
          <SubmitButton className={classes.suggestionSubmit}>
            Submit for Review
          </SubmitButton>
        </form>
      )
    }

    const comboProps = {
      role: 'combobox',
      'aria-haspopup': 'listbox',
      'aria-owns': `${id}-listbox`,
      'aria-expanded': true,
      'aria-controls': `${id}-listbox`,
      onKeyDown: this.onInputKeyDown,
      'aria-autocomplete': 'list',
      'aria-activedescendant':
        suggestions.length && activeIndex >= 0 && suggestions[activeIndex]
          ? `${id}.${suggestions[activeIndex].id}`
          : null,
      ...getAriaProps(others),
    }

    return (
      <React.Fragment>
        <div className={classes.inputContainer}>
          <SearchSvg className={classes.searchSvg} />
          <input
            type="text"
            disabled={disabled}
            className={classes.queryInput}
            placeholder={placeholder}
            value={query}
            onChange={this.onQueryChange}
            onFocus={onInputFocus}
            ref={this.onInputRef}
            {...(isCombo ? comboProps : {})}
          />
          {(isCombo && !isAlwaysCombo) || (query && query.length) ? (
            <CloseButton
              className={classes.clearInputButton}
              onClick={onClearInput}
              aria-label="clear search"
            />
          ) : null}
        </div>
        <div role="alert" className={classes.ariaStatus} ref={this.statusRef} />
        <div
          role="status"
          className={classes.ariaStatus}
          ref={this.filterStatusRef}
        >
          {this.getFilterStatus()}
        </div>
        {suggestions instanceof Promise ? (
          <ListFallback />
        ) : (
          <div
            id={`${id}-listbox`}
            className={classes.listbox}
            role="listbox"
            aria-multiselectable={isMultiSelect}
            ref={this.onListboxRef}
            style={
              addMobileKeyboardPadding && this.listboxRef.current
                ? {
                    paddingBottom: `${this.listboxRef.current.clientHeight -
                      90}px`,
                  }
                : {}
            }
            {...(isAlwaysCombo
              ? {}
              : {
                  tabIndex: 0,
                  onKeyDown: this.onInputKeyDown,
                })}
          >
            {suggestions.map((suggestion, idx) => {
              if (id === 'pickOrg') {
                return (
                  <div
                    role="option"
                    aria-label={`${suggestion.fullName} ${
                      selected[suggestion.uniqueName]
                        ? ', selected'
                        : ', unselected'
                    }`}
                    aria-selected={idx === activeIndex}
                    id={`${id}.${suggestion.uniqueName}`}
                    key={idx}
                    className={classnames(classes.option, {
                      [classes.selected]: selected[suggestion.uniqueName],
                      [classes.active]: idx === activeIndex,
                      [classes.isMember]: suggestion.affiliated,
                    })}
                    onClick={this.makeOnOptionClick(suggestion.uniqueName, idx)}
                    onMouseOver={this.makeOnOptionMouseOver(idx)}
                    ref={this.makeOptionRef(idx)}
                    style={
                      selected[suggestion.uniqueName] && selectedColor
                        ? { color: selectedColor }
                        : null
                    }
                  >
                    {suggestion.fullName}
                    {selected[suggestion.uniqueName] && <CheckSvg />}
                  </div>
                )
              }
              return (
                <div
                  role="option"
                  aria-label={`${suggestion.name} ${
                    selected[suggestion.id] ? ', selected' : ', unselected'
                  }`}
                  aria-selected={idx === activeIndex}
                  id={`${id}.${suggestion.Id}`}
                  key={suggestion.Id}
                  className={classnames(classes.option, {
                    [classes.selected]: selected[suggestion.id],
                    [classes.active]: idx === activeIndex,
                  })}
                  onClick={this.makeOnOptionClick(suggestion.id, idx)}
                  onMouseOver={this.makeOnOptionMouseOver(idx)}
                  ref={this.makeOptionRef(idx)}
                  style={
                    selected[suggestion.id] && selectedColor
                      ? { color: selectedColor }
                      : null
                  }
                >
                  {suggestion.name}
                  {selected[suggestion.id] && <CheckSvg />}
                </div>
              )
            })}
            {!suggestions.length && (
              <div className={classes.noResults}>
                {noResultsMessage.message}
                {noResultsMessage.suggest && (
                  <TransparentButton onClick={this.switchViewMode}>
                    {noResultsMessage.suggest.message}
                  </TransparentButton>
                )}
              </div>
            )}
          </div>
        )}
      </React.Fragment>
    )
  }
  render() {
    const { classes, className, style } = this.props

    return (
      <div
        className={classnames(className, classes.listPicker)}
        style={style}
        onMouseMove={this.onMouseMove}
      >
        {this.renderContent()}
      </div>
    )
  }
}

ListPicker.defaultProps = {
  isCombo: true,
  isAlwaysCombo: true,
  ListFallback: Progress,
}

export default injectSheet(styles)(ListPicker)
