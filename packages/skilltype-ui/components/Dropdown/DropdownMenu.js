import React from 'react'
import classnames from 'classnames'
import injectSheet from 'react-jss'
import MobileOnly from '../Responsive/MobileOnly'
import styles from './styles'

const DropdownMenu = ({ classes, className, children, style }) => (
  <React.Fragment>
    <ul className={classnames(className, classes.menu)} style={style}>
      {children}
    </ul>
    <MobileOnly>
      <div className={classnames('backdrop', classes.backdrop)} />
    </MobileOnly>
  </React.Fragment>
)

export default injectSheet(styles)(DropdownMenu)
