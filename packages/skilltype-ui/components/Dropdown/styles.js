import { macros, theme as defaultTheme } from '../../shared-styles'

export default (theme = defaultTheme) => ({
  container: {
    position: 'relative',
  },
  dropdownOpened: {
    display: 'block',
    ...macros.mobile({
      overflow: 'hidden',
    }),
  },
  toggle: {
    display: 'flex',
    alignItems: 'center',
    border: 'none',
    background: 'white',
    padding: '6px',
    cursor: 'pointer',

    '&:active': {
      background: theme.bgColorLight,
    },
    '&$opened': {
      display: 'flex',
    },
  },
  label: {
    fontSize: '14px',
    padding: '0 6px',
  },
  icon: {
    margin: '0 6px',

    '&:hover': {
      background: theme.bgColorLight,
      borderRadius: '50%',
    },
  },
  menu: {
    position: 'absolute',
    display: 'none',
    margin: '0',
    padding: '0',
    zIndex: '99',
    border: '1px solid #f7f7f7',
    boxShadow: '0 2px 3px 0 rgba(140, 140, 140, 0.5)',
    backgroundColor: '#fff',
    listStyle: 'none',
    right: '0px',
    ...macros.mobile({
      bottom: '0',
      position: 'fixed',
      left: '0',
      right: '0',
      zIndex: '600',
    }),
  },
  backdrop: {
    position: 'fixed',
    display: 'none',
    top: '0',
    left: '0',
    right: '0',
    bottom: '0',
    zIndex: '500',
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
  },
  opened: {
    '& + ul': {
      display: 'block',

      '& + .backdrop': {
        display: 'block',
      },
    },
  },
  item: {
    border: 'none',
    padding: '12px',
    fontSize: '14px',
    background: 'transparent',
    cursor: 'pointer',
    width: '100%',
    minWidth: '200px',
    textAlign: 'left',

    '&:hover': {
      background: theme.bgColorLight,
    },
  },
  separator: {
    height: '1px',
    width: '100%',
    background: theme.bgColorLight,
  },
})
