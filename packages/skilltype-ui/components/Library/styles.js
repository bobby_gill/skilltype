import { macros, theme as defaultTheme } from '../../shared-styles'

export default (theme = defaultTheme) => ({
  resourceListHead: {
    marginBottom: '15px',
    fontFamily: theme.primaryFont,
    display: 'flex',
    flexDirection: 'row',

    '& $content': {
      marginRight: '2em',
    },

    '& $heading': {
      textTransform: 'uppercase',
      color: theme.darkGrey,
    },

    '& $summary': {
      color: theme.black,
    },
  },
  heading: {},
  summary: {},
  resourceList: {
    backgroundColor: theme.white,
    borderRadius: theme.borderRadius,

    '& $resourceListItem': {
      borderBottom: `1px solid ${theme.hairlineGrey}`,

      '&:last-child': {
        borderBottom: 'none',
      },
    },
  },
  resourceItem: {
    display: 'flex',
    flexDirection: 'row',
    textDecoration: 'none',
    padding: theme.padding,

    '& $toggle': {
      display: 'none',
      ...macros.mobile({
        display: 'flex',
      }),
    },

    '&:hover $toggle': {
      display: 'flex',
    },
  },
  resourceItemFallback: {
    '& $icon': {
      ...macros.fallbackContentPlaceholder(theme),
      '& svg': {
        opacity: 0,
      },
    },
    '& $title, & $meta, & $title:active': {
      color: 'rgba(0,0,0,0)',
      height: '1em',
      width: '75%',
      cursor: 'default',
      ...macros.fallbackContentPlaceholder(theme),
    },
    '& $meta': {
      width: '55%',
    },
  },
  resourceListItem: {
    display: 'flex',
    flexDirection: 'column',
    fontFamily: theme.primaryFont,
  },
  resourceInfo: {
    color: theme.darkerGrey,
    margin: theme.padding,
    marginTop: 0,
    maxWidth: theme.maxContentWidth,

    ...macros.mobile(
      {
        marginTop: '0.4em',
        paddingTop: theme.padding,
        borderTop: `1px solid ${theme.lightGrey}`,
      },
      theme
    ),
  },
  icon: {
    paddingTop: '3px',
    marginRight: '12px',
  },

  title: {
    display: 'inline-block',
    color: theme.black,
    textDecoration: 'none',
    marginBottom: '0.6em',

    '&:hover': {
      color: theme.purple,
      textDecoration: 'underline',
    },
    '&:active': {
      backgroundColor: theme.lightPurple,
    },
  },
  meta: {
    color: theme.darkGrey,
    fontSize: theme.fontSizeM,
  },
  content: {
    flexGrow: 1,
  },
  dropdownMenu: {
    padding: '5px',
    minWidth: '56px',

    '& $btnCancel': {
      display: 'block',
      background: theme.lightGrey,
      textAlign: 'center',
      width: 'calc(100% - 24px)',
      ...macros.desktop({
        display: 'none',
      }),
    },

    '& svg': {
      height: '20px',
      width: '20px',
    },

    '& button': {
      cursor: 'pointer',
      display: 'flex',
      alignItems: 'center',
    },

    '& span': {
      marginLeft: '10px',
    },
  },
  toggle: {},
  danger: {
    color: theme.darkRed,

    '& svg': {
      fill: theme.darkRed,
    },
  },
  btnCancel: {
    margin: '12px',
  },
  tagListHead: {
    position: 'relative',
    display: 'flex',
    width: 'calc(100% - 60px)',
    height: '150px',
    padding: '20px 30px',
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    borderRadius: '8px',

    '&:before': {
      content: '""',
      position: 'absolute',
      top: 0,
      right: 0,
      bottom: 0,
      left: 0,
      background:
        'linear-gradient(to top, rgba(0, 0, 0, 0.7) 0%, rgba(0, 0, 0, 0.1) 90%, transparent 100%)',
    },
  },
  tagInfo: {
    position: 'relative',
    color: theme.white,
    fontFamily: theme.primaryFont,
    marginTop: 'auto',
  },
  tagName: {
    fontWeight: 600,
    fontSize: theme.fontSizeXL,
  },
  tagDesc: {
    marginTop: '5px',
  },
})
