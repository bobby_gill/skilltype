import React from 'react'
import classnames from 'classnames'
import injectSheet from 'react-jss'
import TagListHead from './TagListHead'
import styles from './styles'

const ResourceListHead = ({
  tagId,
  tag,
  resourceCount,
  className,
  classes,
  style,
}) => {
  if (tagId) {
    return <TagListHead tag={tag} />
  }

  return (
    <div
      className={classnames(className, classes.resourceListHead)}
      style={style}
    >
      <div className={classes.content}>
        <div role="heading" aria-level="3" className={classes.heading}>
          {`${resourceCount} Item${resourceCount > 1 ? 's' : ''}`}
        </div>
      </div>
    </div>
  )
}

export default injectSheet(styles)(ResourceListHead)
