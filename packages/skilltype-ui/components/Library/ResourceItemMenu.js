import React from 'react'
import injectSheet from 'react-jss'
import PrimaryButton from '@skilltype/ui/components/Button/PrimaryButton'
import Dropdown from '@skilltype/ui/components/Dropdown'
import Modal from '@skilltype/ui/components/Modal/Modal'
import CopyIcon from '@skilltype/ui/assets/dropdown-icons/copy.svg'
import DeleteIcon from '@skilltype/ui/assets/dropdown-icons/delete.svg'
// import HideIcon from '@skilltype/ui/assets/dropdown-icons/hide.svg'
import ReportIcon from '@skilltype/ui/assets/dropdown-icons/report.svg'
import SaveIcon from '@skilltype/ui/assets/dropdown-icons/save.svg'
import { withServiceContext } from '@skilltype/services/components/ServiceProvider'
import { withNotifyContext } from '@skilltype/ui/components/Notify/NotifyProvider'
import styles from './styles'

class ResourceItemMenu extends React.Component {
  state = {
    modalType: null,
  }

  openModal = modalType => {
    this.setState({ modalType })
  }

  closeModal = () => {
    this.setState({ modalType: null })
  }

  updateBookmarkStatus = () => {
    const {
      serviceContext,
      notifyError,
      notifyClose,
      notify,
      resource,
      updateFeed,
    } = this.props

    if (resource.isBookmarked) {
      serviceContext.resource
        .removeFromSaved(resource.id)
        .then(() => {
          updateFeed()
          notify('Resource has been removed from saved items.')
          notifyClose(2000)
        })
        .catch(() => {
          notifyError('Error removing Item from saved items')
          notifyClose(2000)
        })
    } else {
      serviceContext.resource
        .saveResource(resource.id)
        .then(() => {
          updateFeed()
          notify('Resource has been saved.')
          notifyClose(2000)
        })
        .catch(() => {
          notifyError('Error save Item')
          notifyClose(2000)
        })
    }
  }

  copyToClipboard = () => {
    const { serviceContext, notifyClose, notify, resource } = this.props

    const url = `${window.location.protocol}//${window.location.hostname}${
      window.location.port ? `:${window.location.port}` : ''
    }/item/${resource.id}`
    const textField = document.createElement('textarea')
    textField.innerText = url
    document.body.appendChild(textField)
    textField.select()
    document.execCommand('copy')
    textField.remove()
    notify('Link copied to Clipboard.')
    notifyClose(2000)

    serviceContext.resource.shareResource(resource.id)
  }

  hideResource = () => {
    const {
      serviceContext,
      notifyError,
      notifyClose,
      notify,
      resource,
    } = this.props

    serviceContext.resource
      .hideResource(resource.id)
      .then(() => {
        notify('Resource has been hidden.')
        notifyClose(2000)
      })
      .catch(() => {
        notifyError('Error hide Item')
        notifyClose(2000)
      })
    this.closeModal()
  }

  reportItem = () => {
    const {
      serviceContext,
      notifyError,
      notifyClose,
      notify,
      resource,
    } = this.props

    serviceContext.resource
      .reportResource(resource.id)
      .then(() => {
        notify('Resource has been reported.')
        notifyClose(2000)
      })
      .catch(() => {
        notifyError('Error Reporting Item')
        notifyClose(2000)
      })
  }

  deleteItem = () => {
    const {
      serviceContext,
      notifyError,
      notifyClose,
      notify,
      resource,
      isAdmin,
      deleteFeed,
    } = this.props

    if (resource.isOwner || isAdmin) {
      serviceContext.resource
        .deleteResource(resource.id)
        .then(() => {
          deleteFeed()
          notify('Item has been sent to queue for deletion.')
          notifyClose(2000)
        })
        .catch(() => {
          notifyError('Error Deleting Item')
          notifyClose(2000)
        })
    }
    this.closeModal()
  }

  render() {
    const { classes, resource, isAdmin } = this.props
    const { modalType } = this.state

    const modalContent = () => {
      switch (modalType) {
        case 'hide':
          return (
            <Modal
              title="Hide Item"
              prompt
              okButtonLabel="Hide"
              showOkButton
              showCancelButton
              onOk={this.hideResource}
              onDismiss={this.closeModal}
              onCancel={this.closeModal}
              okButtonWide
              shouldCloseOnEsc
              lockBodyScroll
              showCloseButton
            >
              <div
                style={{
                  fontFamily: '"-apple-system", "Helvetica", sans-serif',
                  fontSize: '14px',
                }}
              >
                Are you sure you want to permanently hide this item from your
                feeds?
              </div>
            </Modal>
          )
        case 'delete':
          const okBtn = ({ children, style, ...props }) => (
            <PrimaryButton
              style={{ backgroundColor: '#D0021B', borderColor: '#D0021B' }}
              {...props}
            >
              {children}
            </PrimaryButton>
          )
          return (
            <Modal
              title="Delete Item"
              prompt
              OkButton={okBtn}
              okButtonLabel="Delete"
              showOkButton
              showCancelButton
              onOk={this.deleteItem}
              onDismiss={this.closeModal}
              onCancel={this.closeModal}
              okButtonWide
              shouldCloseOnEsc
              lockBodyScroll
              showCloseButton
            >
              <div
                style={{
                  fontFamily: '"-apple-system", "Helvetica", sans-serif',
                  fontSize: '14px',
                }}
              >
                Are you sure you want to permanently delete this item from your
                feeds?
              </div>
            </Modal>
          )
        default:
          return null
      }
    }

    return (
      <React.Fragment>
        <Dropdown className={classes.dropdownMenu}>
          <Dropdown.Toggle className={classes.toggle} />
          <Dropdown.Menu>
            <Dropdown.Item onClick={this.updateBookmarkStatus}>
              <SaveIcon />
              <span>
                {resource.isBookmarked
                  ? 'Remove from Saved Items'
                  : 'Save to Profile'}
              </span>
            </Dropdown.Item>
            <Dropdown.Item onClick={this.copyToClipboard}>
              <CopyIcon />
              <span>Copy to Clipboard</span>
            </Dropdown.Item>
            {/* <Dropdown.Item onClick={() => this.openModal('hide')}> */}
            {/*  <HideIcon /> <span> Hide from Feeds</span> */}
            {/* </Dropdown.Item> */}
            <Dropdown.Item onClick={this.reportItem}>
              <ReportIcon />
              <span>Report to Skilltype</span>
            </Dropdown.Item>
            {(isAdmin || resource.isOwner) && (
              <Dropdown.Item
                className={classes.danger}
                onClick={() => this.openModal('delete')}
              >
                <DeleteIcon />
                <span>Delete Permanently</span>
              </Dropdown.Item>
            )}
            <Dropdown.Item className={classes.btnCancel}>Cancel</Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
        {modalContent()}
      </React.Fragment>
    )
  }
}

export default withServiceContext(
  withNotifyContext(injectSheet(styles)(ResourceItemMenu))
)
