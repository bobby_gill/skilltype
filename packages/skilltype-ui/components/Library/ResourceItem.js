import React from 'react'
import { string } from 'prop-types'
import classnames from 'classnames'
import injectSheet from 'react-jss'
import {
  HtmlIcon,
  VideoIcon,
  PresentationIcon,
  PodcastIcon,
  PdfIcon,
} from '../../assets/resource-icons'
import styles from './styles'
import { getAriaProps } from '../../lib/props'

const resourceView = resourceType => {
  switch (resourceType) {
    case 'ARTICLE': {
      return {
        icon: <PdfIcon />,
        name: 'Article',
      }
    }
    case 'AUDIO': {
      return {
        icon: <PodcastIcon />,
        name: 'Audio',
      }
    }
    case 'PRESENTATION': {
      return {
        icon: <PresentationIcon />,
        name: 'Presentation',
      }
    }
    case 'VIDEO': {
      return {
        icon: <VideoIcon />,
        name: 'Video',
      }
    }
    default: {
      return {
        icon: <HtmlIcon />,
        name: 'Other',
      }
    }
  }
}

class ResourceItem extends React.Component {
  static propTypes = {
    resourceType: string,
    title: string,
    publishedOn: string,
    source: string,
    url: string,
  }

  onClick = evt => {
    evt.preventDefault()
    if (this.props.onNavigate) {
      this.props.onNavigate(evt)
    }
  }

  render() {
    const {
      id,
      resourceType,
      source,
      title,
      publishedOn,
      url,
      classes,
      className,
      style,
      ...others
    } = this.props
    const view = resourceView(resourceType)
    const children = React.Children.map(others.children, child =>
      React.cloneElement(child, { classes })
    )
    return (
      <div
        className={classnames(className, classes.resourceItem)}
        style={style}
        {...getAriaProps(others)}
      >
        <div className={classes.icon}>{view.icon}</div>
        <div className={classes.content}>
          <a
            className={classes.title}
            href={`/item/${id}`}
            onClick={this.onClick}
          >
            {title}
          </a>

          <div className={classes.meta}>
            {source} · {publishedOn.toString()}
          </div>
        </div>
        {children}
      </div>
    )
  }
}

export default injectSheet(styles)(ResourceItem)
