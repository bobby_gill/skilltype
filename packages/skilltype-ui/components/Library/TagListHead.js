import React from 'react'
import injectSheet from 'react-jss'
import classnames from 'classnames'
import styles from './styles'

const TagListHead = ({ tag, className, classes }) => (
  <div
    className={classnames(className, classes.tagListHead)}
    style={{
      backgroundImage:
        'url(https://uploads-ssl.webflow.com/5d73005a34bd367a51de8161/5e1fcce22b16e2f464f534e3_dia_art_1_topic_page_header.jpg)',
    }}
  >
    {tag && (
      <div className={classes.tagInfo}>
        <div className={classes.tagName}>{tag.displayName}</div>
        {tag.description && (
          <div className={classes.tagDesc}>{tag.description}</div>
        )}
      </div>
    )}
  </div>
)

export default injectSheet(styles)(TagListHead)
