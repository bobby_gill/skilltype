import React from 'react'
import FormField from '../FormField'
import Select from '../../Select/Select'

const CountryField = ({ options, searchable, ...props }) => (
  <FormField {...props}>
    <Select
      options={[
        {
          value: 'AU',
          label: 'Australia',
        },
        {
          value: 'CA',
          label: 'Canada',
        },
        {
          value: 'NZ',
          label: 'New Zealand',
        },
        {
          value: 'UK',
          label: 'United Kingdom',
        },
        {
          value: 'US',
          label: 'United States',
        },
      ]}
      searchable={searchable}
      {...props}
    />
  </FormField>
)

export default CountryField
