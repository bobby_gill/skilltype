```js
initialState = {
  values: {
    countryCode: 'US',
  },
  inline: false,
}
;<Section>
  <Form
    id="textField1"
    onChange={({ values }) => setState({ values })}
    values={state.values}
  >
    <FormSection>
      <CountryField
        id="countryCode"
        label="Country"
        inline={state.inline}
      />
    </FormSection>
  </Form>
  <Checkbox
    value={state.inline}
    onChange={e => setState({ inline: e.target.value })}
    label="Inline"
  />
</Section>
```
