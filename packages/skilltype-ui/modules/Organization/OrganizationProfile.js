import React from 'react'
import Modal from '@skilltype/ui/components/Modal/Modal'
import Progress from '@skilltype/ui/components/Progress/Progress'
import TagListPicker from '@skilltype/ui/components/TagList/TagListPicker'
import ProfileSection from '@skilltype/ui/components/ProfileSection/ProfileSection'
import InstructionSection from '@skilltype/ui/components/Section/InstructionSection'
import TagList from '@skilltype/ui/components/TagList/TagList'
import Tag from '@skilltype/ui/components/Tag/Tag'
import InlineLinkButton from '@skilltype/ui/components/Button/InlineLinkButton'
import { alphabetizeTagList } from '@skilltype/ui/lib/tags'
import { prefixSearch } from '@skilltype/services/lib/suggestions'
import organizationMeta from '@skilltype/data/data/organization-profile-sections.json'
import { withLocationContext } from '@skilltype/ui/components/Router/Router'
import { withServiceContext } from '@skilltype/services/components/ServiceProvider'
import colors from '../../shared-styles/colors'

const ProfileSections = withLocationContext(
  ({ tagGroups, onEditTagGroup, adminMode, locationContext }) =>
    Object.keys(tagGroups).map(groupKey => {
      let tagsList
      if (tagGroups[groupKey].tagLinkable && tagGroups[groupKey].category) {
        tagsList = tagGroups[groupKey].category.tags
          .sort((a, b) => (a.displayName < b.displayName ? -1 : 1))
          .map(tag => (
            <Tag key={tag.id}>
              <InlineLinkButton
                style={{ color: colors.purple }}
                navigate={locationContext.navigate}
                href={tag.href}
              >
                {tag.displayName}
              </InlineLinkButton>
            </Tag>
          ))
      } else {
        tagsList = alphabetizeTagList(tagGroups[groupKey].tags).map(tag => (
          <Tag key={tag.id}>{tag.name}</Tag>
        ))
      }
      return (
        <ProfileSection
          title={tagGroups[groupKey].title}
          key={groupKey}
          canEdit={adminMode ? tagGroups[groupKey].canEdit : false}
          onEdit={() => onEditTagGroup(groupKey)}
        >
          {tagGroups[groupKey].tags.length ? (
            <TagList tagTheme="white">{tagsList}</TagList>
          ) : (
            <InstructionSection>
              {tagGroups[groupKey].placeholder}
            </InstructionSection>
          )}
        </ProfileSection>
      )
    })
)

class Profile extends React.Component {
  static propTypes = {}
  state = {
    editGroupKey: null,
    editedTagList: [],
    suggestions: [],
    querySuggestions: [],
    query: '',
  }
  onModalOk = () => {
    const values = Object.keys(this.tagGroups).reduce((dict, key) => {
      if (key === this.state.editGroupKey) {
        dict[key] = this.state.editedTagList.map(tag => tag.name)
        return dict
      }
      dict[key] = this.tagGroups[key].tags.map(tag => tag.name)
      return dict
    }, {})
    this.props.saveProfile(values).then(success => {
      if (!success) {
        return
      }
      this.tagGroups = this.props.getTagGroups(
        this.props.organizationProfile,
        organizationMeta
      )
      this.setState({
        editGroupKey: null,
      })
    })
  }
  onModalDismiss = () => {
    this.setState({
      editGroupKey: null,
      editedTagList: [],
    })
  }
  onTagListChange = editedTagList => {
    this.setState({
      editedTagList,
    })
  }
  onEditTagGroup = groupKey => {
    const tagGroup = this.tagGroups[groupKey]
    if (groupKey === 'memberships') {
      this.setState({
        editGroupKey: groupKey,
        editedTagList: [],
        querySuggestions: [],
        query: '',
        suggestions: [],
      })
      return this.props.serviceContext.organization
        .getMemberships()
        .then(({ content }) => {
          const suggestions = content.map(item => ({
            id: item.id,
            name: item.displayName,
          }))
          return this.setState({
            editedTagList: tagGroup.category.tags.map(i => ({
              id: i.id,
              name: i.displayName,
            })),
            querySuggestions: suggestions,
            suggestions,
          })
        })
    }

    const suggestions = this.props.suggestionsByType[tagGroup.suggestionsType]

    return this.setState({
      editGroupKey: groupKey,
      editedTagList: tagGroup.tags,
      querySuggestions: suggestions,
      query: '',
      suggestions,
    })
  }
  onTagQueryChange = query => {
    this.setState({
      query,
      querySuggestions: prefixSearch(this.state.suggestions, query),
    })
  }
  suggestTag = (name, cb) => {
    const { editGroupKey } = this.state
    const { suggestTag } = this.props

    if (
      !editGroupKey ||
      !this.tagGroups[editGroupKey] ||
      !this.tagGroups[editGroupKey].category
    )
      return

    const category = this.tagGroups[editGroupKey].category
    suggestTag(
      {
        categoryId: category.id,
        name,
      },
      cb
    )
  }
  tagGroups = this.props.getTagGroups(
    this.props.organizationProfile,
    organizationMeta
  )

  render() {
    const { adminMode, isSaving } = this.props
    const { editGroupKey } = this.state
    const tagGroup = editGroupKey && this.tagGroups[editGroupKey]
    return (
      <React.Fragment>
        <ProfileSections
          adminMode={adminMode}
          tagGroups={this.tagGroups}
          onEditTagGroup={this.onEditTagGroup}
        />
        {tagGroup && (
          <Modal
            title={`Edit ${tagGroup.title}`}
            appElementId="root"
            okButtonLabel="Update"
            onOk={this.onModalOk}
            onDismiss={this.onModalDismiss}
            okIsEnabled={!isSaving}
            cancelIsEnabled={!isSaving}
            shouldCloseOnEsc
            lockBodyScroll
            showCancelButton
            showOkButton
            showCloseButton
            hasEditableContent
            contentHandlesScroll
            wide
          >
            {isSaving && <Progress />}
            <TagListPicker
              id="profileTagListPicker"
              tagList={this.state.editedTagList}
              onChange={this.onTagListChange}
              suggestTag={this.suggestTag}
              suggestions={this.state.querySuggestions}
              query={this.state.query}
              onQueryChange={this.onTagQueryChange}
              queryPlaceholder={tagGroup.searchPlaceholder}
              noResultsMessage={tagGroup.noResults}
              noTagsMessage={tagGroup.noTags.message}
              disabled={isSaving}
              style={{ flexGrow: 1 }}
              focusOnMount
            />
          </Modal>
        )}
      </React.Fragment>
    )
  }
}

export default withServiceContext(Profile)
