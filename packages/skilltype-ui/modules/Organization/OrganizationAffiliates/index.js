import React from 'react'
import injectSheet from 'react-jss'
import { withStyles } from '@material-ui/core/styles'
import RaisedSection from '@skilltype/ui/components/Section/RaisedSection'
import { withServiceContext } from '@skilltype/services/components/ServiceProvider'
import { withNotifyContext } from '@skilltype/ui/components/Notify/NotifyProvider'
import Table from '@skilltype/ui/components/Table/Table'
import Dropdown from '@skilltype/ui/components/Dropdown'
import SearchSvg from '@skilltype/ui/assets/search.svg'
import OrganizationInviteUsers from './OrganizationInviteUsers'
import styles from '../styles'

// We can inject some CSS into the DOM.
const tableStyle = {
  root: {
    '& > thead > tr >  th:first-child': {
      display: 'none',
    },
    '& > tbody > tr >  td:first-child': {
      display: 'none',
    },
    '& > thead > tr > th:nth-child(3)': {
      width: '50px',
    },
    '& > tbody > tr > td:nth-child(3)': {
      width: '50px',
    },
  },
}

class Affiliates extends React.Component {
  static defaultProps = {
    onUpdate: () => {},
  }

  state = {
    affiliates: this.props.affiliates,
    query: '',
    isPending: false,
  }

  onUpdate = (role, affiliateId) => {
    if (this.state.isPending) return

    const { id, notify, notifyClose, notifyError } = this.props
    const { affiliates } = this.state
    const idx = affiliates.findIndex(item => item.id === affiliateId)

    if (idx < 0) return

    const successMessage = 'Role updated.'
    const errorMessage = "Owner's role cannot be changed"

    if (affiliates[idx].owner) {
      notifyError(errorMessage)
      notifyClose(2000)
      return
    }

    this.setState({ isPending: true })

    this.props.serviceContext.organization
      .updateAffiliateRelationShip(id, affiliateId, 'Mensagem', role)
      .then(() => {
        if (affiliates[idx].relationship !== role) {
          affiliates[idx].revokedAccessDate = !role ? new Date() : null
        }
        affiliates[idx].relationship = role
        this.setState({ isPending: false, affiliates })
        notify(successMessage)
        notifyClose(2000)
      })
      .catch(() => {
        this.setState({ isPending: false })
        notifyError(errorMessage)
        notifyClose(2000)
      })
  }

  onQueryChange = ({ target: { value: query } }) => {
    const affiliates = this.props.affiliates.filter(user =>
      user.affiliate.toLowerCase().includes(query)
    )
    this.setState({ query, affiliates })
  }

  render() {
    const { theme, classes } = this.props
    const { affiliates } = this.state

    const roleOptions = [
      { label: 'Manager', value: 'MANAGER' },
      { label: 'Member', value: 'MEMBER' },
      { label: 'Follower', value: 'FOLLOWER' },
    ]
    return (
      <React.Fragment>
        <RaisedSection
          className={classes.affiliatesTable}
          title={`Affiliates ${affiliates ? `(${affiliates.length})` : ``}`}
        >
          <div className={this.props.classes.inputContainer}>
            <SearchSvg className={this.props.classes.searchSvg} />
            <input
              type="text"
              className={this.props.classes.queryInput}
              placeholder="Search"
              value={this.state.query}
              onChange={this.onQueryChange}
            />
          </div>
          <Table
            classes={{ root: classes.root }}
            data={affiliates.map(item => [
              item.id,
              item.affiliate,
              item.relationship,
              item.revokedAccessDate ? 'Inactive' : 'Active',
            ])}
            maxHeight="100%"
            columns={[
              { name: 'Id' },
              { name: 'Name' },
              {
                name: 'Role',
                options: {
                  customBodyRender: (value, updateValue, row, column) => (
                    <Dropdown
                      className={classes.dropdown}
                      key={`TableCell-Select-${row[0]}-${column}`}
                    >
                      <Dropdown.Toggle className={classes.ddToggle}>
                        {value
                          ? roleOptions.find(o => o.value === value).label
                          : 'Revoked'}
                      </Dropdown.Toggle>
                      <Dropdown.Menu>
                        {roleOptions.map(item => (
                          <Dropdown.Item
                            key={item.value}
                            onClick={() => this.onUpdate(item.value, row[0])}
                          >
                            {item.label}
                          </Dropdown.Item>
                        ))}
                        <Dropdown.Item
                          onClick={() => this.onUpdate(null, row[0])}
                        >
                          <span className={classes.danger}>Revoke</span>
                        </Dropdown.Item>
                      </Dropdown.Menu>
                    </Dropdown>
                  ),
                },
              },
              {
                name: 'Status',
                options: {
                  customBodyRender: value => {
                    const status = value.toLowerCase()
                    const colorMapping = {
                      active: theme.green,
                      inactive: theme.darkRed,
                      pending: theme.yellow,
                    }
                    const color = colorMapping[status] || theme.black
                    return <div style={{ color }}>{value}</div>
                  },
                },
              },
            ]}
          />
        </RaisedSection>
        <OrganizationInviteUsers />
      </React.Fragment>
    )
  }
}

export default withStyles(tableStyle)(
  injectSheet(styles)(withServiceContext(withNotifyContext(Affiliates)))
)
