import React from 'react'
import ProfileSection from '@skilltype/ui/components/ProfileSection/ProfileSection'
import InstructionSection from '@skilltype/ui/components/Section/InstructionSection'
import TagList from '@skilltype/ui/components/TagList/TagList'
import Tag from '@skilltype/ui/components/Tag/Tag'
import Content from '@skilltype/ui/components/Viewport/Content'
import InlineLinkButton from '@skilltype/ui/components/Button/InlineLinkButton'
import { withLocationContext } from '@skilltype/ui/components/Router/Router'
import { alphabetizeTagList } from '../../lib/tags'
import colors from '../../shared-styles/colors'

const Profile = ({ tagGroups, onEditTagGroup, locationContext }) => (
  <Content>
    {Object.keys(tagGroups).map(groupKey => {
      let tagsList
      if (tagGroups[groupKey].tagLinkable && tagGroups[groupKey].category) {
        tagsList = tagGroups[groupKey].category.tags
          .sort((a, b) => (a.displayName < b.displayName ? -1 : 1))
          .map(tag => (
            <Tag key={tag.id}>
              <InlineLinkButton
                style={{ color: colors.purple }}
                navigate={locationContext.navigate}
                href={tag.href}
              >
                {tag.displayName}
              </InlineLinkButton>
            </Tag>
          ))
      } else {
        tagsList = alphabetizeTagList(tagGroups[groupKey].tags).map(tag => (
          <Tag key={tag.id}>{tag.name}</Tag>
        ))
      }

      return (
        <ProfileSection
          key={groupKey}
          title={tagGroups[groupKey].title}
          canEdit={tagGroups[groupKey].canEdit}
          onEdit={() => onEditTagGroup(groupKey)}
        >
          {tagGroups[groupKey].tags.length ? (
            <TagList
              tagTheme="white"
              aria-label={`Tag list for "${tagGroups[groupKey].title}"`}
            >
              {tagsList}
            </TagList>
          ) : (
            <InstructionSection>
              {tagGroups[groupKey].placeholder}
            </InstructionSection>
          )}
        </ProfileSection>
      )
    })}
  </Content>
)

export default withLocationContext(Profile)
