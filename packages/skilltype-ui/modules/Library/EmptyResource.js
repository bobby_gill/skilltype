import React from 'react'
import injectSheet from 'react-jss'
import classnames from 'classnames'
import Section from '@skilltype/ui/components/Section/Section'
import TagListHead from '@skilltype/ui/components/Library/TagListHead'
import InlineLinkButton from '@skilltype/ui/components/Button/InlineLinkButton'
import {
  pathFromUri,
  withLocationContext,
} from '@skilltype/ui/components/Router/Router'
import FeedSvg from '@skilltype/ui/assets/feed.svg'
import styles from './styles'

const EmptyResource = ({ tagId, tag, classes, locationContext }) => {
  const onNavigate = e => locationContext.navigate(pathFromUri(e.target.href))

  if (tagId) {
    return (
      <React.Fragment>
        <TagListHead className={classes.emptyTagHeader} tag={tag} />
        {tag && (
          <Section
            className={classnames(classes.emptyContainer, classes.tagPage)}
          >
            <div className={classes.header}>
              <span className={classes.title}>
                Welcome to the {tag.displayName} Feed
              </span>
            </div>
            <div className={classes.description}>
              Topic feeds help you identify resources and opportunities for a
              specific skill or interest. As items are added, {"they'll"} appear
              here in a searchable and filterable list. To get started, you
              should&nbsp;
              <InlineLinkButton
                className={classes.linkBtn}
                href="/profile"
                onClick={onNavigate}
              >
                add an item
              </InlineLinkButton>
              .
            </div>
          </Section>
        )}
      </React.Fragment>
    )
  }
  return (
    <Section className={classes.emptyContainer}>
      <div className={classes.header}>
        <FeedSvg />
        <span className={classes.title}>Welcome to your Feed</span>
      </div>
      <div className={classes.description}>
        It seems you {"haven't"} added any tags to your profile. Once you do,
        relevant resources and opportunities will appear here. Start
        updating&nbsp;
        <InlineLinkButton
          className={classes.linkBtn}
          href="/profile"
          onClick={onNavigate}
        >
          your profile
        </InlineLinkButton>
        &nbsp;now.
      </div>
    </Section>
  )
}

export default injectSheet(styles)(withLocationContext(EmptyResource))
