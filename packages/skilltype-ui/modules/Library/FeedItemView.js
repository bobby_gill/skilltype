import React from 'react'
import injectSheet from 'react-jss'
import moment from 'moment'
import { withLocationContext } from '@skilltype/ui/components/Router/Router'
import { withServiceContext } from '@skilltype/services/components/ServiceProvider'
import { withNotifyContext } from '@skilltype/ui/components/Notify/NotifyProvider'
import RaisedSection from '@skilltype/ui/components/Section/RaisedSection'
import Tag from '@skilltype/ui/components/Tag/Tag'
import TransparentButton from '@skilltype/ui/components/Button/TransparentButton'
import InlineLinkButton from '@skilltype/ui/components/Button/InlineLinkButton'
import ResourceItemMenu from '@skilltype/ui/components/Library/ResourceItemMenu'
import { domainFromUrl } from '@skilltype/ui/lib/url'
import {
  HtmlIcon,
  PdfIcon,
  PodcastIcon,
  PresentationIcon,
  VideoIcon,
} from '@skilltype/ui/assets/resource-icons'
import ArrowIcon from '@skilltype/ui/assets/arrow.svg'
import styles from './styles'

const resIcon = resourceType => {
  switch (resourceType) {
    case 'ARTICLE': {
      return <PdfIcon />
    }
    case 'AUDIO': {
      return <PodcastIcon />
    }
    case 'PRESENTATION': {
      return <PresentationIcon />
    }
    case 'VIDEO': {
      return <VideoIcon />
    }
    default: {
      return <HtmlIcon />
    }
  }
}

const FeedItemView = ({
  serviceContext,
  locationContext,
  classes,
  resource,
  isAdmin,
  updateFeed,
  notify,
  notifyError,
  notifyClose,
}) => {
  const reportResource = () => {
    serviceContext.resource
      .reportResource(resource.id)
      .then(() => {
        notify('Item has been reported successfully.')
        notifyClose(2000)
      })
      .catch(e => {
        console.error(e)
        notifyError('Error while reporting item')
        notifyClose(2000)
      })
  }

  const updateItem = () => {
    if (resource.isBookmarked) {
      updateFeed({
        ...resource,
        isBookmarked: false,
        bookmarkedCount: resource.bookmarkedCount - 1,
      })
    } else {
      updateFeed({
        ...resource,
        isBookmarked: true,
        bookmarkedCount: resource.bookmarkedCount + 1,
      })
    }
  }

  return (
    <React.Fragment>
      <div className={classes.breadcrumb}>
        <TransparentButton
          className={classes.btnBack}
          onClick={() => locationContext.navigate('/feed')}
        >
          <ArrowIcon />
        </TransparentButton>
      </div>
      <RaisedSection>
        <div className={classes.resItemHeader}>
          <div className={classes.resItemSummary}>
            <div className={classes.resItemIcon}>
              {resIcon(resource.mediaType)}
            </div>
            <div className={classes.resItemHeaderInfo}>
              <a href={resource.sourceUrl} target="_blank">
                {resource.displayName}
              </a>
              <div className={classes.greyLabel}>
                <span>
                  {domainFromUrl(new URL(resource.sourceUrl).hostname)}
                </span>
                <span className={classes.separator}>·</span>
                <span>{moment(resource.publicationDate).fromNow()}</span>
              </div>
            </div>
            <ResourceItemMenu
              resource={resource}
              isAdmin={isAdmin}
              updateFeed={updateItem}
              deleteFeed={() => locationContext.navigate('/feed')}
            />
          </div>
          <div className={classes.resItemTags}>
            {resource.tags.map(tag => (
              <Tag key={tag.id}>
                <InlineLinkButton
                  className={classes.tagInnerLink}
                  navigate={locationContext.navigate}
                  href={`/tag/${tag.id}`}
                >
                  {tag.displayName}
                </InlineLinkButton>
              </Tag>
            ))}
          </div>
        </div>
        <div className={classes.resItemStatus}>
          <span>Added ({resource.sharedCount})</span>
          <span className={classes.separator}>·</span>
          <span>Saved ({resource.bookmarkedCount})</span>
          <span className={classes.separator}>·</span>
          <TransparentButton
            className={classes.btnReport}
            onClick={reportResource}
          >
            Report
          </TransparentButton>
        </div>
        <div className={classes.resItemDescription}>
          {resource.description && (
            <React.Fragment>
              <p>{resource.description}</p>
              <div className={classes.greyLabel}>
                <span>{resource.createdUser}</span>
                <span className={classes.separator}>·</span>
                <span>{moment(resource.createdAt).fromNow()}</span>
              </div>
            </React.Fragment>
          )}
        </div>
      </RaisedSection>
    </React.Fragment>
  )
}

export default withServiceContext(
  withNotifyContext(withLocationContext(injectSheet(styles)(FeedItemView)))
)
