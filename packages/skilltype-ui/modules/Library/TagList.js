import React from 'react'
import injectSheet from 'react-jss'
import classnames from 'classnames'
import { withLocationContext } from '@skilltype/ui/components/Router/Router'
import { withNotifyContext } from '@skilltype/ui/components/Notify/NotifyProvider'
import { withServiceContext } from '@skilltype/services/components/ServiceProvider'
import styles from './styles'

class TagList extends React.Component {
  state = {
    tags: [],
  }

  componentDidMount() {
    const { serviceContext, notifyError, notifyClose } = this.props

    serviceContext.resource
      .getTags()
      .then(res => {
        this.setState({ tags: res })
      })
      .catch(err => {
        console.error(err)
        notifyError('Failed get tags.')
        notifyClose(2000)
      })
  }

  onSelectItem = item => {
    this.props.locationContext.navigate(`/tag/${item.id}`)
  }

  render() {
    const { classes, tagId } = this.props
    const { tags } = this.state

    return (
      <div className={classes.tagList}>
        {tags.map((item, idx) => (
          <button
            key={idx}
            className={classnames(classes.tagListItem, {
              [classes.active]: item.id === tagId,
            })}
            onClick={() => this.onSelectItem(item)}
          >
            {item.displayName}
          </button>
        ))}
      </div>
    )
  }
}

export default withServiceContext(
  withLocationContext(withNotifyContext(injectSheet(styles)(TagList)))
)
