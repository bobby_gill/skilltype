import { theme as defaultTheme } from '../../shared-styles'

export default (theme = defaultTheme) => ({
  emptyContainer: {
    padding: '40px',
    fontFamily: theme.primaryFont,
    backgroundColor: theme.white,
    borderRadius: theme.borderRadius,
    border: `2px solid ${theme.hairlineGrey}`,
    maxWidth: theme.wideModalWidth,
  },
  emptyTagHeader: {
    maxWidth: theme.wideModalWidth,
  },
  tagPage: {
    border: 'none',

    '&$emptyContainer': {
      padding: '40px 30px',
    },
    '& $header': {
      justifyContent: 'center',
    },
    '& $title': {
      margin: 0,
      textAlign: 'center',
    },
    '& $description': {
      maxWidth: '430px',
      marginLeft: 'auto',
      marginRight: 'auto',
      textAlign: 'center',
    },
  },
  header: {
    display: 'flex',
    alignItems: 'center',
  },
  title: {
    fontWeight: 800,
    fontSize: theme.fontSizeL,
    marginLeft: '10px',
  },
  description: {
    marginTop: '1rem',
  },
  linkBtn: {
    color: theme.purple,
    textDecoration: 'none',
  },
  footer: {
    paddingTop: '1rem',
    color: theme.disabledGrey,
    fontSize: theme.fontSizeS,
    fontFamily: theme.primaryFont,

    '& $linkBtn': {
      fontSize: theme.fontSizeS,
    },
  },
  breadcrumb: {
    display: 'flex',
    alignItems: 'center',
    marginBottom: '10px',
    fontFamily: theme.primaryFont,
  },
  btnBack: {
    padding: '0 10px',

    '& svg': {
      width: '15px',
    },
  },
  btnReport: {
    fontSize: '14px',
    color: theme.darkRed,
  },
  resItemHeader: {
    padding: '15px 20px',
    borderBottom: `1px solid ${theme.hairlineGrey}`,
  },
  resItemSummary: {
    display: 'flex',
    paddingBottom: '15px',
  },
  resItemTags: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  resItemHeaderInfo: {
    flex: '1',
    paddingLeft: '8px',

    '& a': {
      fontFamily: theme.primaryFont,
      fontSize: theme.fontSizeL,
      fontWeight: 500,
      color: theme.black,
      textDecoration: 'none',
    },
  },
  resItemIcon: {
    minWidth: '30px',
  },
  greyLabel: {
    color: theme.disabledGrey,
    fontFamily: theme.primaryFont,
    marginTop: '10px',
  },
  separator: {
    padding: '0 5px',
  },
  resItemStatus: {
    display: 'flex',
    padding: '15px 20px',
    fontFamily: theme.primaryFont,
  },
  resItemDescription: {
    padding: '15px 20px',
    fontFamily: theme.primaryFont,
    borderTop: `1px solid ${theme.hairlineGrey}`,

    '& p': {
      margin: 0,
      wordBreak: 'break-all',
    },
  },
  tagInnerLink: {
    color: theme.black,
  },
  tagList: {
    padding: '10px',
  },
  tagListItem: {
    border: 'none',
    background: 'transparent',
    fontSize: '16px',
    padding: '10px 5px',
    width: '100%',
    textAlign: 'left',
    cursor: 'pointer',

    '&:hover': {
      background: theme.white,
    },
    '&$active': {
      background: theme.mediumGrey,
    },
  },
  active: {},
})
