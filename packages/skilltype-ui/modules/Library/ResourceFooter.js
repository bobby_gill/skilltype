import React from 'react'
import injectSheet from 'react-jss'
import InlineLinkButton from '@skilltype/ui/components/Button/InlineLinkButton'
import {
  pathFromUri,
  withLocationContext,
} from '../../components/Router/Router'
import styles from './styles'

const ResourceFooter = ({ classes, locationContext }) => {
  const onNavigate = e => locationContext.navigate(pathFromUri(e.target.href))

  return (
    <div className={classes.footer}>
      <strong>Tip:</strong> The feed shows you links from&nbsp;
      <InlineLinkButton
        className={classes.linkBtn}
        href="/profile"
        onClick={onNavigate}
      >
        topics
      </InlineLinkButton>
      &nbsp;and organizations you&nbsp;
      <InlineLinkButton
        className={classes.linkBtn}
        href="/profile"
        onClick={onNavigate}
      >
        follow
      </InlineLinkButton>
      . Learn more at our&nbsp;
      <InlineLinkButton
        className={classes.linkBtn}
        href="http://help.skilltype.com"
        target="_blank"
      >
        Help Center
      </InlineLinkButton>
      .
    </div>
  )
}

export default injectSheet(styles)(withLocationContext(ResourceFooter))
