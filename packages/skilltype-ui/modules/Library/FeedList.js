import React from 'react'
import { arrayOf, shape, number, string, bool, func } from 'prop-types'
import ResourceListHead from '@skilltype/ui/components/Library/ResourceListHead'
import ResourceList from '@skilltype/ui/components/Library/ResourceList'
import ResourceListItem from '@skilltype/ui/components/Library/ResourceListItem'
import ResourceItem from '@skilltype/ui/components/Library/ResourceItem'
import ResourceItemMenu from '@skilltype/ui/components/Library/ResourceItemMenu'
import { withServiceContext } from '@skilltype/services/components/ServiceProvider'
import { withNotifyContext } from '@skilltype/ui/components/Notify/NotifyProvider'
import EmptyResource from './EmptyResource'

class FeedList extends React.Component {
  static propTypes = {
    totalCount: number,
    resourceList: arrayOf(shape({})),
    tagId: string,
    isAdmin: bool,
    updateFeed: func,
    deleteFeed: func,
    onNavigate: func,
    changePageTitle: func,
  }

  state = {
    tag: null,
  }

  componentDidMount() {
    this.fetchTag(this.props.tagId)
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.tagId !== nextProps.tagId) {
      this.fetchTag(nextProps.tagId)
    }
  }

  fetchTag = tagId => {
    if (tagId) {
      const {
        serviceContext,
        notifyError,
        notifyClose,
        changePageTitle,
      } = this.props

      serviceContext.resource
        .getTagById(tagId)
        .then(res => {
          this.setState({ tag: res })
          changePageTitle(res.displayName)
        })
        .catch(err => {
          console.error(err)
          notifyError('Failed get tag information.')
          notifyClose(2000)
        })
    } else {
      this.setState({
        tag: null,
      })
    }
  }

  updateFeed = resource => {
    const { updateFeed } = this.props

    if (resource.isBookmarked) {
      updateFeed(resource.id, {
        ...resource,
        isBookmarked: false,
        bookmarkedCount: resource.bookmarkedCount - 1,
      })
    } else {
      updateFeed(resource.id, {
        ...resource,
        isBookmarked: true,
        bookmarkedCount: resource.bookmarkedCount - 1,
      })
    }
  }

  deleteFeed = resource => {
    const { deleteFeed } = this.props

    deleteFeed(resource.id)
  }

  makeOnNavigate = url => () => {
    if (this.props.onNavigate) {
      this.props.onNavigate(url)
    }
  }

  render() {
    const { resourceList, totalCount, tagId, isAdmin } = this.props
    const { tag } = this.state

    if (totalCount === 0) {
      return <EmptyResource tagId={tagId} tag={tag} />
    }
    return (
      <React.Fragment>
        <ResourceListHead tagId={tagId} tag={tag} resourceCount={totalCount} />
        <ResourceList>
          {resourceList.map((res, idx) => (
            <ResourceListItem key={idx}>
              <ResourceItem
                {...res}
                onNavigate={this.makeOnNavigate(`/item/${res.id}`)}
              >
                <ResourceItemMenu
                  resource={res}
                  isAdmin={isAdmin}
                  updateFeed={() => this.updateFeed(res)}
                  deleteFeed={() => this.deleteFeed(res)}
                />
              </ResourceItem>
            </ResourceListItem>
          ))}
        </ResourceList>
      </React.Fragment>
    )
  }
}

export default withServiceContext(withNotifyContext(FeedList))
