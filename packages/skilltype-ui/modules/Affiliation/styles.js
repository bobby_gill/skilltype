import { theme as defaultTheme } from '../../shared-styles'

export default (theme = defaultTheme) => ({
  btnPrimary: {
    display: 'block',
    fontFamily: theme.primaryFont,
    fontSize: '14px',
    color: theme.textColorDark,
    cursor: 'pointer',
    padding: '0',
    background: 'none',
    border: 'none',
    borderRadius: '0',
    outline: 'none',
    textDecoration: 'none',
  },
  success: {
    color: theme.green,
    fontFamily: theme.primaryFont,
  },
})
