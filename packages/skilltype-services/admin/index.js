import {
  httpDelete,
  httpGet,
  httpPatch,
  httpPost,
  httpPut,
} from '../lib/requestApi'

export const getUsersWithRoles = () => httpGet('/skilltype-admin/roles/users')

export const updateUserRole = ({ id, roleName }) =>
  httpPut(`/skilltype-admin/users/${id}/role`, { roleName })
export const inviteUsersOrganization = organizationId =>
  httpGet(`/user/invites/organization/${organizationId}`)
export const getCategories = () => httpGet('/tags/categories')
export const getTagsByCategory = (id, page) =>
  httpGet(`/tags/categories/${id}/tags?page=${page}`)
export const createTag = data => httpPost('/tags', data)
export const updateTag = (id, data) => httpPut(`/tags/${id}`, data)
export const deleteTag = id => httpDelete(`/tags/${id}`)
export const getTagSuggestions = () => httpGet('/tag-suggestions')
export const convertSuggestionToTag = id => httpPatch(`/tag-suggestions/${id}`)
export const discardTagSuggestion = id => httpDelete(`/tag-suggestions/${id}`)
