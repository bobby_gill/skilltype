import { getJson, addClientVersionQs } from '@skilltype/services/lib/request'

export const getAffiliationPermissions = () =>
  getJson(addClientVersionQs(process.env.REACT_APP_AFFILIATION_PERMISSIONS_URL))
