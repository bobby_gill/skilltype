import {
  httpGet,
  httpPostFormData,
  httpDelete,
  httpPost,
  httpPatch,
} from '../lib/requestApi'

export const getResource = () => httpGet('/resources')

export const getFeeds = (pageNumber, bookmarked, tagId, types) => {
  let params = `sort=publicationDate,desc&page=${pageNumber}&size=20`
  if (types.length > 0) {
    params += `&itemTypes=${types.join(',')}`
  }
  if (tagId) {
    params += `&tagId=${tagId}`
  }
  if (bookmarked) {
    params += '&onlyBookmarked=true'
  }

  return httpGet(`/feed?${params}`)
}

export const getFeedById = id => httpGet(`/feed/${id}`)

export const bulkInsertResource = data =>
  httpPostFormData(`/resources/bulk`, data)

export const getReportedResource = () => httpGet('/reported-resources')

export const saveResource = id => httpPost(`/feed/${id}/bookmark`)
export const removeFromSaved = id => httpDelete(`/feed/${id}/bookmark`)
export const hideResource = id => httpPost(`/feed/${id}/hide`)
export const shareResource = id => httpPost(`/feed/${id}/share`)
export const reportResource = id => httpPost(`/feed/${id}/report`)
export const deleteResource = id => httpDelete(`/feed/${id}`)

export const removeFromReportList = id =>
  httpPatch(`/reported-resources/${id}`, { action: 'REMOVE' })

export const getTags = () => httpGet(`/feed/tags`)
export const getTagById = id => httpGet(`/tags/${id}`)
export const getItemTypes = () => httpGet(`/feed/item-types`)
