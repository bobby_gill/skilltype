import React from 'react'
import injectSheet from 'react-jss'
import { withLocationContext } from '@skilltype/ui/components/Router/Router'
import FeedList from '@skilltype/ui/modules/Library/FeedList'
import TagList from '@skilltype/ui/modules/Library/TagList'
import ResourceFooter from '@skilltype/ui/modules/Library/ResourceFooter'
import Progress from '@skilltype/ui/components/Progress/Progress'
import Header from '../App/Header'
import Body from '../App/Body'
import RightSidebar from '../App/rightSidebar'
import { withUserContext, userIsRoleSkilltypeAdmin } from '../User/UserProvider'
import styles from './styles'

class ResourceViewWithSave extends React.Component {
  state = {
    title: this.props.title,
    isLoading: false,
  }

  componentDidMount(): void {
    this.props.userContext.setInitialFeedStatus(() => {
      this.props.userContext.selectItemType([], this.fetchData)
    })
  }

  componentWillReceiveProps(nextProps): void {
    const prevItems = this.props.userContext.itemTypesSelected
    const nextItems = nextProps.userContext.itemTypesSelected
    const isEqualItems =
      prevItems.length === nextItems.length &&
      prevItems.sort().every((val, idx) => val === nextItems.sort()[idx])

    if (!isEqualItems || this.props.tagId !== nextProps.tagId) {
      this.refreshData()
    }
  }

  componentWillUnmount(): void {
    if (this.onScroll) {
      window.removeEventListener('scroll', this.onScroll)
      this.onScroll = null
    }
  }

  onScroll = null

  fetchData = () => {
    const { userContext, tagId } = this.props

    this.setState({
      isLoading: true,
    })

    userContext.fetchFeeds(tagId, false, res => {
      this.setState({ isLoading: false }, () => {
        if (!res.last) {
          if (!this.onScroll) {
            this.onScroll = () => {
              if (this.state.isLoading) return

              if (
                window.pageYOffset + window.innerHeight >=
                document.body.scrollHeight - 100
              ) {
                this.fetchData()
              }
            }
            window.addEventListener('scroll', this.onScroll)
          }
        } else if (this.onScroll) {
          window.removeEventListener('scroll', this.onScroll)
          this.onScroll = null
        }
      })
    })
  }

  refreshData = () => {
    this.props.userContext.setInitialFeedStatus(this.fetchData)
  }

  changePageTitle = title => {
    this.setState({ title })
  }

  render() {
    const {
      classes,
      userContext: {
        user,
        feeds,
        totalFeedCount,
        updateFeedById,
        deleteFeedById,
      },
      tagId,
    } = this.props
    const { title } = this.state

    return (
      <React.Fragment>
        <Header title={title} />
        <Body userData={user} feedPage>
          {this.state.isLoading && <Progress />}
          {(!this.state.isLoading || feeds.length > 0) && (
            <div className={classes.content}>
              <FeedList
                resourceList={feeds}
                totalCount={totalFeedCount}
                tagId={tagId}
                isAdmin={userIsRoleSkilltypeAdmin(user)}
                onNavigate={url => this.props.locationContext.navigate(url)}
                updateFeed={updateFeedById}
                deleteFeed={deleteFeedById}
                changePageTitle={this.changePageTitle}
              />
              <ResourceFooter />
            </div>
          )}
          <RightSidebar>
            {tagId ? null : <TagList tagId={parseInt(tagId, 10)} />}
          </RightSidebar>
        </Body>
      </React.Fragment>
    )
  }
}

export default withLocationContext(
  withUserContext(injectSheet(styles)(ResourceViewWithSave))
)
