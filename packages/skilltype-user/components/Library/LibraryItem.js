import React from 'react'
import injectSheet from 'react-jss'
import Progress from '@skilltype/ui/components/Progress/Progress'
import FeedItemView from '@skilltype/ui/modules/Library/FeedItemView'
import { withUserContext, userIsRoleSkilltypeAdmin } from '../User/UserProvider'
import Header from '../App/Header'
import Body from '../App/Body'
import RightSidebar from '../App/rightSidebar'
import styles from './styles'

class LibraryItem extends React.Component {
  componentDidMount() {
    this.fetchData()
  }

  fetchData = () => {
    const { id, userContext } = this.props

    userContext.fetchFeedById(id)
  }

  render() {
    const {
      classes,
      title,
      userContext: { user, feed, updateFeed },
    } = this.props

    return (
      <React.Fragment>
        <Header title={title} />
        <Body userData={user}>
          <div className={classes.content}>
            {feed ? (
              <FeedItemView
                resource={feed}
                isAdmin={userIsRoleSkilltypeAdmin(user)}
                updateFeed={updateFeed}
              />
            ) : (
              <Progress />
            )}
          </div>
          <RightSidebar />
        </Body>
      </React.Fragment>
    )
  }
}

export default withUserContext(injectSheet(styles)(LibraryItem))
