import React from 'react'
import injectSheet from 'react-jss'
import { withLocationContext } from '@skilltype/ui/components/Router/Router'
import FeedList from '@skilltype/ui/modules/Library/FeedList'
import ResourceFooter from '@skilltype/ui/modules/Library/ResourceFooter'
import Progress from '@skilltype/ui/components/Progress/Progress'
import Header from '../App/Header'
import Body from '../App/Body'
import RightSidebar from '../App/rightSidebar'
import { withUserContext, userIsRoleSkilltypeAdmin } from '../User/UserProvider'
import styles from './styles'

class SavedItems extends React.Component {
  state = {
    isLoading: false,
  }

  componentDidMount(): void {
    this.props.userContext.setInitialFeedStatus(this.fetchData)
  }

  componentWillUnmount(): void {
    if (this.onScroll) {
      window.removeEventListener('scroll', this.onScroll)
      this.onScroll = null
    }
  }

  onScroll = null

  fetchData = () => {
    const { userContext } = this.props

    this.setState({
      isLoading: true,
    })

    userContext.fetchFeeds(null, true, res => {
      this.setState({ isLoading: false }, () => {
        if (!res.last) {
          if (!this.onScroll) {
            this.onScroll = () => {
              if (this.state.isLoading) return

              if (
                window.pageYOffset + window.innerHeight >=
                document.body.scrollHeight - 100
              ) {
                this.fetchData()
              }
            }
            window.addEventListener('scroll', this.onScroll)
          }
        } else if (this.onScroll) {
          window.removeEventListener('scroll', this.onScroll)
          this.onScroll = null
        }
      })
    })
  }

  render() {
    const {
      classes,
      title,
      userContext: {
        user,
        feeds,
        totalFeedCount,
        updateFeedById,
        deleteFeedById,
      },
      tagId,
    } = this.props

    return (
      <React.Fragment>
        <Header title={title} />
        <Body userData={user}>
          {this.state.isLoading && <Progress />}
          {(!this.state.isLoading || feeds.length > 0) && (
            <div className={classes.content}>
              <FeedList
                resourceList={feeds.filter(item => item.isBookmarked)}
                totalCount={totalFeedCount}
                tagId={tagId}
                isAdmin={userIsRoleSkilltypeAdmin(user)}
                onNavigate={url => this.props.locationContext.navigate(url)}
                updateFeed={updateFeedById}
                deleteFeed={deleteFeedById}
              />
              <ResourceFooter />
            </div>
          )}
          <RightSidebar />
        </Body>
      </React.Fragment>
    )
  }
}

export default withLocationContext(
  withUserContext(injectSheet(styles)(SavedItems))
)
