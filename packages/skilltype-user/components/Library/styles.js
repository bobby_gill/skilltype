import { macros, theme as defaultTheme } from '@skilltype/ui'

export default (theme = defaultTheme) => ({
  content: {
    flex: 1,

    ...macros.desktop(
      {
        marginRight: '275px',
      },
      theme
    ),
  },
})
