import React from 'react'
import { string } from 'prop-types'
import { withAwait } from '@skilltype/ui/components/Await/Await'
import Affiliates from '@skilltype/ui/modules/Organization/OrganizationAffiliates'
import { withOrganizationContext } from '../OrganizationProvider'

// TODO @jacob: Create affilaites fallback table
const OrganizationAffiliates = withAwait(Affiliates)

// TODO @jacob: Enable loader when affiliates endpoints are deployed
// TODO @jacob: Provide onUpdate callback function for affiliation status changing

class OrganizationDirectoryLoader extends React.Component {
  static propTypes = {
    id: string.isRequired,
  }

  /*
   * OrganizationDirectoryLoader depends on the affiliates relation to the current organization (by id)
   */
  constructor(props) {
    super(props)
    props.organizationContext.fetchOrganizationAffiliates(props.id)
  }

  render() {
    const { organizationContext, ...restProps } = this.props
    const { organizationAffiliates } = organizationContext

    if (!organizationAffiliates) {
      return null
    }

    return (
      <OrganizationAffiliates
        affiliates={organizationAffiliates}
        {...restProps}
      />
    )
  }
}

export default withOrganizationContext(OrganizationDirectoryLoader)
