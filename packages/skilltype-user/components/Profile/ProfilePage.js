import React from 'react'
import { withServiceContext } from '@skilltype/services/components/ServiceProvider'
import profileMeta from '@skilltype/data/data/profile-sections.json'
import affiliationPermissions from '@skilltype/data/data/affiliation-permissions.json'
import Header from '../App/Header'
import Body from '../App/Body'
import Profile from './ProfileLoader'
import { withUserContext } from '../User/UserProvider'

class ProfilePage extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      profileMeta,
      affiliationPermissions,
    }
  }
  componentWillMount() {
    this.props.userContext.fetchProfile()
  }
  render() {
    const { userContext } = this.props
    const { firstName, lastName } = userContext.user
    const menuTitle = `${firstName} ${lastName}`

    return (
      <React.Fragment>
        <Header title={menuTitle} />
        <Body userData={userContext.user}>
          <Profile
            affiliationPermissions={this.state.affiliationPermissions}
            profileData={userContext.profile}
            profileMeta={this.state.profileMeta}
          />
        </Body>
      </React.Fragment>
    )
  }
}

export default withUserContext(withServiceContext(ProfilePage))
