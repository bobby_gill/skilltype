import React from 'react'
import moment from 'moment'
import { Redirect } from '@reach/router'
import { withNotifyContext } from '@skilltype/ui/components/Notify/NotifyProvider'
import { withServiceContext } from '@skilltype/services/components/ServiceProvider'
import Progress from '@skilltype/ui/components/Progress/ProgressPortal'
import bugsnagClient from '@skilltype/services/lib/bugsnag'
import { domainFromUrl } from '@skilltype/ui/lib/url'

const { Consumer, Provider } = React.createContext()

class UserProvider extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      user: props.user,
      profile: null,
      feed: null,
      feeds: [],
      totalFeedCount: 0,
      itemTypesSelected: [],
      isSaving: false,
    }
    this.userContext = {
      saveUser: this.saveUser,
      fetchProfile: this.fetchProfile,
      saveProfile: this.saveProfile,
      saveSettings: this.saveSettings,
      changePassword: this.changePassword,
      requestPasswordReset: this.requestPasswordReset,
      confirmEmail: this.confirmEmail,
      validateResetToken: this.validateResetToken,
      resetPassword: this.resetPassword,
      inviteUsersOrganization: this.inviteUsersOrganization,
      inviteUsers: this.inviteUsers,
      fetchFeeds: this.fetchFeeds,
      fetchFeedById: this.fetchFeedById,
      fetchFeedItemTypes: this.fetchFeedItemTypes,
      setInitialFeedStatus: this.setInitialFeedStatus,
      selectItemType: this.selectItemType,
      updateFeed: this.updateFeed,
      updateFeedById: this.updateFeedById,
      deleteFeedById: this.deleteFeedById,
      suggestTag: this.suggestTag,
      connectOrganization: this.connectOrganization,
      disconnectOrganization: this.disconnectOrganization,
    }
  }
  setInitialFeedStatus = cb => {
    this.page = 0
    this.setState(
      {
        feeds: [],
        totalFeedCount: 0,
      },
      cb
    )
  }
  selectItemType = (itemTypes, cb) => {
    this.setState(
      {
        itemTypesSelected: itemTypes,
      },
      cb
    )
  }
  page = 0
  saveUser = async (
    values,
    successMessage = 'User has been updated.',
    errorMessage = 'Error updating user. Please try again later.'
  ) => {
    const { serviceContext, notify, notifyError } = this.props
    this.setState({ isSaving: true })
    try {
      await serviceContext.user.save(values)
      this.setState({ user: values, isSaving: false })
      notify(successMessage)
    } catch (err) {
      notifyError(errorMessage)
    }
  }
  fetchProfile = () => {
    // if profile is already populated, skip fetch
    if (!this.state.profile) {
      this.setState({
        profile: this.props.serviceContext.user
          .getProfile()
          .then(profile => this.setState({ profile })),
      })
    }
  }
  saveProfile = async values => {
    const { serviceContext, notify, notifyError } = this.props
    this.setState({ isSaving: true })
    try {
      await serviceContext.user.saveProfile(values)
      this.setState({
        isSaving: false,
        profile: await this.props.serviceContext.user.getProfile(),
      })
      notify('Profile has been updated.')
      return true
    } catch (err) {
      console.error(err)
      notifyError('Error updating profile. Please try again later.')
      this.setState({ isSaving: false })
      return false
    }
  }
  fetchFeeds = (tagId = null, bookmarked = false, cb) => {
    const { serviceContext, notifyError, notifyClose } = this.props
    let { feeds } = this.state
    const { itemTypesSelected } = this.state

    serviceContext.resource
      .getFeeds(this.page, bookmarked, tagId, itemTypesSelected)
      .then(res => {
        this.page += 1
        feeds = feeds.concat(
          res.content.map(item => ({
            id: item.id,
            resourceType: item.mediaType,
            title: item.displayName,
            publishedOn: moment(item.publicationDate).fromNow(),
            url: item.sourceUrl,
            source: domainFromUrl(new URL(item.sourceUrl).hostname),
            publishedDate: moment(item.publicationDate),
            isBookmarked: item.isBookmarked,
            isOwner: item.isOwner,
          }))
        )
        this.setState({ feeds, totalFeedCount: res.totalElements }, () =>
          cb(res)
        )
      })
      .catch(err => {
        console.error(err)
        notifyError('Failed to get feeds.')
        notifyClose(2000)
      })
  }
  fetchFeedById = id => {
    const { serviceContext, notifyError, notifyClose } = this.props

    serviceContext.resource
      .getFeedById(id)
      .then(res => {
        this.setState({ feed: res })
      })
      .catch(err => {
        console.error(err)
        notifyError('Failed to get feed.')
        notifyClose(2000)
      })
  }
  fetchFeedItemTypes = cb => {
    const { serviceContext, notifyError, notifyClose } = this.props

    serviceContext.resource
      .getItemTypes()
      .then(res => {
        cb(res)
      })
      .catch(err => {
        console.error(err)
        notifyError('Failed to get item types.')
        notifyClose(2000)
      })
  }
  updateFeed = feed => {
    this.setState({ feed })
  }
  updateFeedById = (id, value) => {
    const { feeds } = this.state
    const idx = feeds.map(item => item.id).indexOf(id)
    if (idx >= 0) {
      feeds[idx] = value
      this.setState({ feeds })
    }
  }
  deleteFeedById = id => {
    const feeds = [...this.state.feeds]

    const idx = feeds.map(item => item.id).indexOf(id)
    if (idx >= 0) {
      feeds.splice(idx, 1)
      this.setState({ feeds })
    }
  }
  saveSettings = async (
    values,
    successMessage = 'Settings updated successfully.',
    errorMessage = 'Error updating settings. Please try again later.'
  ) => {
    const { serviceContext, notify, notifyError } = this.props
    this.setState({ isSaving: true })
    try {
      const user = await serviceContext.user.saveSettings(values)
      this.setState({ isSaving: false, user })
      notify(successMessage)
      return true
    } catch (err) {
      console.error(err)
      notifyError(errorMessage)
      this.setState({ isSaving: false })
      return false
    }
  }
  suggestTag = (data, cb) => {
    const { serviceContext, notify, notifyError } = this.props
    this.setState({ isSaving: true })

    serviceContext.user
      .createTagSuggestion(data)
      .then(() => {
        this.setState({ isSaving: false })
        notify('Tag has been suggested successfully.')
        if (cb) {
          cb()
        }
      })
      .catch(err => {
        console.error(err)
        notifyError('Error while suggesting tag. Please try again later.')
        this.setState({ isSaving: false })
      })
  }
  changePassword = async values => {
    const { serviceContext, notify, notifyError } = this.props
    this.setState({ isSaving: true })

    try {
      await serviceContext.user.changePassword(values)
      this.setState({ isSaving: false })
      notify('Password has been updated successfully.')
      return true
    } catch (err) {
      console.error(err)
      notifyError('Error updating password. Please try again later.')
      this.setState({ isSaving: false })
      return false
    }
  }
  requestPasswordReset = async values => {
    const { serviceContext } = this.props
    try {
      await serviceContext.user.requestPasswordReset(values)
      return true
    } catch (err) {
      console.error(err)
      return false
    }
  }
  validateResetToken = async token => {
    const { serviceContext, notifyError, notifyClose } = this.props
    try {
      await serviceContext.user.validateResetToken(token)
      return true
    } catch (err) {
      console.error(err)
      notifyError('Invalid Password Reset Token')
      notifyClose(2000)
      return false
    }
  }
  confirmEmail = token => {
    const { serviceContext } = this.props
    return serviceContext.user.confirmEmail(token)
  }
  resetPassword = async (values, token) => {
    const { serviceContext, notify, notifyError } = this.props
    this.setState({ isSaving: true })
    try {
      await serviceContext.user.resetPassword(values, token)
      this.setState({ isSaving: false })
      notify('Password has been reset successfully.')
      return true
    } catch (err) {
      console.error(err)
      notifyError(
        'Your password reset token is either invalid or expired. Please request another one'
      )
      this.setState({ isSaving: false })
      return false
    }
  }

  inviteUsersOrganization = async values => {
    const { serviceContext, notify, notifyClose } = this.props

    this.setState({ isSaving: true })

    try {
      await serviceContext.user.inviteUsersOrganization(values)

      this.setState({ isSaving: false })
      notify('Invites successfully sent.')
      notifyClose(2000)
      return true
    } catch (err) {
      console.error(err)
      this.setState({ isSaving: false })
      return false
    }
  }

  inviteUsers = async values => {
    const { serviceContext, notify, notifyClose } = this.props

    this.setState({ isSaving: true })

    try {
      await serviceContext.user.inviteUsers(values)

      this.setState({ isSaving: false })
      notify('Invites successfully sent.')
      notifyClose(2000)
      return true
    } catch (err) {
      console.error(err)
      this.setState({ isSaving: false })
      return false
    }
  }

  connectOrganization = async (organizationId, relationship) => {
    const { serviceContext, notify, notifyError, notifyClose } = this.props

    this.setState({ isSaving: true })
    try {
      await serviceContext.organization.requestAffiliation({
        organizationId,
        relationship,
      })
      notify('Your request has been submitted successfully.')
      notifyClose(2000)
      const profile = await this.props.serviceContext.user.getProfile()
      this.setState({
        isSaving: false,
        profile,
      })
    } catch (err) {
      console.error(err)
      notifyError('Failed to send request to the organization')
      notifyClose(2000)
    }
  }

  disconnectOrganization = async id => {
    const { notify, notifyError, notifyClose } = this.props

    this.setState({ isSaving: true })
    try {
      await this.props.serviceContext.organization.disconnectAffiliation({
        organizationId: id,
      })
      notify('You have been disconnected.')
      notifyClose(2000)
      const profile = await this.props.serviceContext.user.getProfile()
      this.setState({
        isSaving: false,
        profile,
      })
    } catch (e) {
      console.error(e)
      notifyError('Error Disconnecting Affiliation')
      notifyClose(2000)
    }
  }

  render() {
    const userContext = {
      ...this.userContext,
      ...this.state,
    }
    // set bugsnag user
    bugsnagClient.user = {
      email: this.props.user.email,
      name: `${this.props.user.firstName} ${this.props.user.lastName}`,
    }
    return (
      <Provider value={userContext}>
        {this.state.isSaving && <Progress />}
        {this.props.children}
      </Provider>
    )
  }
}

export default withNotifyContext(withServiceContext(UserProvider))

export const withUserContext = Wrapped => props => (
  <Consumer>
    {context => {
      if (!context) {
        console.warn(
          'withUserContext component not wrapped in a UserProvider',
          Wrapped
        )
        return <Wrapped {...props} />
      }
      return <Wrapped {...props} userContext={context} />
    }}
  </Consumer>
)

/**
 * User Roles
 * - Helper functions
 */

export const ROLE_USER = 'ROLE_USER'
export const ROLE_CUSTOMER = 'ROLE_CUSTOMER'
export const ROLE_ADMIN = 'ROLE_ADMIN'

export const isRole = (user, role) => user.role.name === role

// TODO @jacob: Update the org permission checking with the organizationId, once it's live on the backend
export const isRoleOrganization = user => isRole(user, ROLE_CUSTOMER)

// Check user role
export const userIsRoleUser = user => isRole(user, ROLE_USER)
export const userIsRoleOrgAdmin = user => isRole(user, ROLE_CUSTOMER)
export const userIsRoleAdmin = user => isRole(user, ROLE_CUSTOMER)
export const userIsRoleSkilltypeAdmin = user => isRole(user, ROLE_ADMIN)

// Generic GuardComponent for any role
const GuardRole = withUserContext(
  ({ roleCheckFn, redirect = '/', userContext, children }) => {
    if (roleCheckFn(userContext.user)) {
      return <React.Fragment>{children}</React.Fragment>
    }
    /**
     * <Redirect> component's noThrow will prevent the tree starting at /admin
     * from rendering and will start over. This prevents a development
     * environemnt error from Create React App
     *
     * Documentation: https://reach.tech/router/api/Redirect#noThrow
     */
    return <Redirect from="/" to={redirect} noThrow />
  }
)

export const GuardRoleUser = ({ redirect, children }) => (
  <GuardRole roleCheckFn={userIsRoleUser} redirect={redirect}>
    {children}
  </GuardRole>
)

export const GuardRoleOrgAdmin = ({ orgId, redirect, children }) => (
  <GuardRole roleCheckFn={userIsRoleOrgAdmin(orgId)} redirect={redirect}>
    {children}
  </GuardRole>
)

export const GuardRoleAdmin = ({ redirect, children }) => (
  <GuardRole roleCheckFn={userIsRoleOrgAdmin} redirect={redirect}>
    {children}
  </GuardRole>
)

export const GuardRoleSkilltypeAdmin = ({ redirect, children }) => (
  <GuardRole roleCheckFn={userIsRoleSkilltypeAdmin} redirect={redirect}>
    {children}
  </GuardRole>
)
