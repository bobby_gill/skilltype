import React, { Component } from 'react'
import injectSheet from 'react-jss'
import cn from 'classnames'
import Form from '@skilltype/ui/components/Form/Form'
import SubmitButton from '@skilltype/ui/components/Button/SubmitButton'
import Modal from '@skilltype/ui/components/Modal/Modal'
import { withNotifyContext } from '@skilltype/ui/components/Notify/NotifyProvider'
import Dropdown from '@skilltype/ui/components/Dropdown'
import { withServiceContext } from '@skilltype/services/components/ServiceProvider'
import RemoveIcon from '@skilltype/ui/assets/remove.svg'
import TagsPicker from './TagsPicker'
import organizationTypes from '../Organization/data/organization.types'
import styles from './styles'

class TagsForm extends Component {
  state = {
    tags: [],
    categorySelected: null,
    tagSelected: null,
    values: {
      displayName: '',
      description: '',
    },
    file: null,
    isLastTags: true,
    showModal: false,
  }

  onNewTag = () => {
    const { categorySelected } = this.state

    if (
      categorySelected &&
      (categorySelected.uniqueName === 'affiliations' ||
        categorySelected.uniqueName === 'memberships')
    ) {
      this.setState({
        tagSelected: null,
        values: {
          displayName: '',
          type: null,
          location: '',
        },
      })
    } else {
      this.setState({
        tagSelected: null,
        values: {
          displayName: '',
          description: '',
        },
      })
      this.removeFile()
    }
  }

  onEditTag = tag => {
    const { categorySelected } = this.state

    if (
      categorySelected &&
      (categorySelected.uniqueName === 'affiliations' ||
        categorySelected.uniqueName === 'memberships')
    ) {
      this.setState({
        tagSelected: tag,
        values: {
          displayName: tag.displayName,
          type: null,
          location: '',
        },
      })
    } else {
      this.removeFile()
      this.setState({
        tagSelected: tag,
        values: {
          displayName: tag.displayName,
          description: tag.description,
        },
        file: tag.headerPhoto
          ? {
              name: tag.headerPhoto,
            }
          : null,
      })
    }
  }

  onDeleteTag = tag => {
    this.setState({
      tagSelected: tag,
      showModal: true,
    })
  }

  page = 0

  handleChange = e => {
    const { values } = this.state
    values[e.target.id] = e.target.value
    this.setState({ values })
  }

  handleFileChange = e => {
    const file = e.target.files[0]
    this.setState({ file })
  }

  removeFile = () => {
    document.getElementById('headerPhoto').value = null
    this.setState({ file: null })
  }

  selectCategory = category => {
    let values = {
      displayName: '',
      description: '',
    }
    if (
      category.uniqueName === 'affiliations' ||
      category.uniqueName === 'memberships'
    ) {
      values = {
        displayName: '',
        type: null,
        location: '',
      }
    }

    this.setState(
      {
        tags: [],
        isLastTags: true,
        categorySelected: category,
        values,
      },
      () => {
        if (
          category.uniqueName !== 'affiliations' &&
          category.uniqueName !== 'memberships'
        ) {
          this.removeFile()
        }
        this.page = 0
        this.fetchTags()
      }
    )
  }

  fetchTags = () => {
    const { categorySelected } = this.state
    const { serviceContext, notifyError, notifyClose } = this.props

    serviceContext.admin
      .getTagsByCategory(categorySelected.id, this.page)
      .then(res => {
        this.page += 1
        const tags = this.state.tags.concat(
          res.content.map(item => ({
            id: item.id,
            displayName: item.displayName,
            description: item.description ? item.description : '',
          }))
        )
        this.setState({
          tags,
          isLastTags: res.last,
        })
      })
      .catch(err => {
        console.error(err)
        notifyError('Failed to get tags.')
        notifyClose(2000)
      })
  }

  loadMore = () => {
    const { isLastTags } = this.state

    if (isLastTags) return

    this.fetchTags()
  }

  handleSubmit = (e, { isValid }) => {
    if (!isValid) return
    const { categorySelected, tagSelected, values } = this.state
    const { serviceContext, notify, notifyError, notifyClose } = this.props

    if (tagSelected) {
      serviceContext.admin
        .updateTag(tagSelected.id, {
          categoryId: categorySelected.id,
          name: values.displayName,
          description: values.description,
        })
        .then(() => {
          const { tags } = this.state
          const idx = tags.map(item => item.id).indexOf(tagSelected.id)
          if (idx >= 0) {
            tags[idx] = {
              ...tags[idx],
              ...values,
            }
            this.setState({ tags })
          }
          notify('Tag has been updated successfully.')
          notifyClose(2000)
        })
        .catch(err => {
          console.error(err)
          notifyError('Failed to update the tag.')
          notifyClose(2000)
        })
    } else {
      serviceContext.admin
        .createTag({
          categoryId: categorySelected.id,
          name: values.displayName,
          description: values.description,
        })
        .then(() => {
          this.setState({
            values: {
              displayName: '',
              description: '',
            },
            file: null,
          })
          document.getElementById('headerPhoto').value = null
          notify('Tag has been created successfully.')
          notifyClose(2000)
        })
        .catch(err => {
          console.error(err)
          notifyError('Failed to create a new tag.')
          notifyClose(2000)
        })
    }
  }

  deleteTag = () => {
    const { serviceContext, notify, notifyError, notifyClose } = this.props
    const { tagSelected } = this.state

    this.closeModal()
    serviceContext.admin
      .deleteTag(tagSelected.id)
      .then(() => {
        const tags = this.state.tags.filter(item => item.id !== tagSelected.id)
        this.setState({ tags })
        notify('Tag has been deleted successfully.')
        notifyClose(2000)
      })
      .catch(err => {
        console.error(err)
        notifyError('Failed to delete a tag.')
        notifyClose(2000)
      })
  }

  closeModal = () => {
    this.setState({
      tagSelected: null,
      showModal: false,
    })
  }

  render() {
    const {
      values,
      file,
      tags,
      categorySelected,
      tagSelected,
      showModal,
    } = this.state
    const { classes, categories } = this.props
    let form

    if (
      categorySelected &&
      (categorySelected.uniqueName === 'affiliations' ||
        categorySelected.uniqueName === 'memberships')
    ) {
      const type = organizationTypes.find(item => item.value === values.type)
      form = (
        <React.Fragment>
          <div className={classes.formGroup}>
            <label htmlFor="displayName">Type</label>
            <Dropdown className={classes.dropdown}>
              <Dropdown.Toggle className={classes.ddToggle}>
                {type ? type.label : organizationTypes[0].label}
              </Dropdown.Toggle>
              <Dropdown.Menu>
                {organizationTypes.map(item => (
                  <Dropdown.Item
                    key={item.value}
                    onClick={() =>
                      this.handleChange({
                        target: { id: 'type', value: item.value },
                      })
                    }
                  >
                    {item.label}
                  </Dropdown.Item>
                ))}
              </Dropdown.Menu>
            </Dropdown>
          </div>
          <div className={classes.formGroup}>
            <label htmlFor="location">Lat/Long</label>
            <input
              type="text"
              id="location"
              onChange={this.handleChange}
              value={values.location}
            />
          </div>
        </React.Fragment>
      )
    } else {
      form = (
        <React.Fragment>
          <div className={classes.formGroup}>
            <label htmlFor="headerPhoto">Header Photo</label>
            <div className={classes.imageUpload}>
              <label
                htmlFor="headerPhoto"
                style={{ margin: '0' }}
                className={classes.btnPrimary}
              >
                Upload
              </label>
              {file && (
                <React.Fragment>
                  <span className={classes.imageName}>{file.name}</span>
                  <button
                    className={classes.btnPrimary}
                    onClick={this.removeFile}
                  >
                    <RemoveIcon />
                  </button>
                </React.Fragment>
              )}
              <input
                id="headerPhoto"
                type="file"
                style={{ display: 'none' }}
                onChange={this.handleFileChange}
                accept=".csv"
              />
            </div>
          </div>
          <div className={classes.verticalFormGroup}>
            <div className={classes.formHeading}>
              <label htmlFor="description">Description</label>
              <span>{values.description.length}/160</span>
            </div>
            <textarea
              id="description"
              className={classes.descriptionInput}
              rows={4}
              maxLength={160}
              onChange={this.handleChange}
              value={values.description}
            />
          </div>
        </React.Fragment>
      )
    }

    return (
      <div className={classes.tagFormContent}>
        <div className={classes.tagForm}>
          <div className={classes.categoriesList}>
            {categories.map(category => (
              <button
                key={category.id}
                className={cn(classes.category, {
                  [classes.active]:
                    categorySelected && category.id === categorySelected.id,
                })}
                onClick={() => this.selectCategory(category)}
              >
                {category.name}
              </button>
            ))}
          </div>
          <Form
            id="tags-form"
            values={values}
            onSubmit={this.handleSubmit}
            disableSubmitUntilValid={false}
            style={{ padding: '8px 0 16px 0' }}
          >
            <div className={classes.formGroup}>
              <label htmlFor="displayName">Display Name</label>
              <input
                type="text"
                id="displayName"
                onChange={this.handleChange}
                value={values.displayName}
              />
            </div>
            {form}
            <SubmitButton
              disabled={!categorySelected}
              style={{ margin: '0 15px' }}
            >
              {tagSelected ? 'Update Tag' : 'Create Tag'}
            </SubmitButton>
          </Form>
        </div>
        <div className={classes.tagsPicker}>
          <TagsPicker
            tags={tags}
            onNewTag={this.onNewTag}
            onEditTag={this.onEditTag}
            onDeleteTag={this.onDeleteTag}
            loadMore={this.loadMore}
          />
        </div>
        {showModal && (
          <Modal
            title="Delete Tag"
            prompt
            showOkButton
            okButtonLabel="Delete"
            showCloseButton
            showCancelButton
            onOk={this.deleteTag}
            onDismiss={this.closeModal}
            onCancel={this.closeModal}
          >
            <div className={classes.confirmMessage}>
              Are you sure you would like to delete {tagSelected.displayName}?
            </div>
          </Modal>
        )}
      </div>
    )
  }
}

export default injectSheet(styles)(
  withNotifyContext(withServiceContext(TagsForm))
)
