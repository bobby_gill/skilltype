import React, { Component } from 'react'
import injectSheet from 'react-jss'
import PlusIcon from '@skilltype/ui/assets/plus.svg'
import styles from './styles'

class TagsPicker extends Component {
  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.tags !== prevState.tags) {
      return { query: '', tags: nextProps.tags }
    }

    return null
  }

  state = {
    query: '',
    tags: this.props.tags,
  }

  onQueryChange = e => {
    this.setState({
      query: e.target.value,
    })
  }

  getFilteredTags = () => {
    const { query, tags } = this.state

    return tags.filter(s =>
      s.displayName
        .split(' ')
        .reduce(
          (found, w) =>
            found ||
            query.toLowerCase() === w.slice(0, query.length).toLowerCase(),
          false
        )
    )
  }

  handleTags = ({ target }) => {
    const { loadMore } = this.props

    if (target.scrollTop >= target.scrollHeight - target.clientHeight) {
      loadMore()
    }
  }

  render() {
    const { query } = this.state
    const { classes, onNewTag, onEditTag, onDeleteTag } = this.props
    const tags = this.getFilteredTags()

    return (
      <React.Fragment>
        <div className={classes.tagQueryInput}>
          <input
            type="text"
            value={query}
            placeholder="Start typing to filter..."
            onChange={this.onQueryChange}
          />
          <button className={classes.btnPrimary} onClick={onNewTag}>
            <PlusIcon />
          </button>
        </div>
        <div className={classes.tagsList} onScroll={this.handleTags}>
          {tags.map((tag, idx) => (
            <div key={idx} className={classes.tagItem}>
              <span className={classes.tagName}>{tag.displayName}</span>
              <div className={classes.tagAction}>
                <button
                  className={classes.btnPrimary}
                  onClick={() => onEditTag(tag)}
                >
                  Edit
                </button>
                <span>·</span>
                <button
                  className={classes.btnDanger}
                  onClick={() => onDeleteTag(tag)}
                >
                  Delete
                </button>
              </div>
            </div>
          ))}
        </div>
      </React.Fragment>
    )
  }
}

export default injectSheet(styles)(TagsPicker)
