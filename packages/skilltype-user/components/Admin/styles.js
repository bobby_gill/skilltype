import { theme as defaultTheme, macros } from '@skilltype/ui/shared-styles'

export default (theme = defaultTheme) => ({
  sectionHeader: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    margin: 0,
    padding: '20px',
    borderBottom: `1px solid ${theme.hairlineGrey}`,
    fontSize: '14px',
  },
  title: {
    fontFamily: theme.primaryFont,
    fontSize: theme.fontSizeL,
  },
  inputContainer: {
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'center',
    flexShrink: 0,
    ...macros.mobileOrMobileOs(
      {
        padding: '0.5em',
        backgroundColor: theme.lightGrey,
      },
      theme
    ),
  },
  queryInput: {
    border: 'none',
    borderRadius: 0,
    borderBottom: `1px solid ${theme.mediumGrey}`,
    '-webkit-appearance': 'none',
    lineHeight: 'normal',
    padding: '1.3em 35px',
    display: 'flex',
    alignItems: 'center',
    fontFamily: theme.primaryFont,
    fontSize: theme.fontSizeNormal,
    backgroundColor: theme.white,

    ...macros.mobileOrMobileOs(
      {
        padding: '0.5em 28px',
        fontSize: theme.fontSizeMobileInput,
        borderRadius: '3px',
        borderBottom: 'none',
      },
      theme
    ),
  },
  searchSvg: {
    width: '16px',
    height: '16px',
    position: 'absolute',
    left: '18px',
    '& path, & circle': {
      stroke: theme.darkGrey,
    },
    ...macros.tabletNotMobileOs(
      {
        left: '13px',
      },
      theme
    ),
  },
  uploadItemsSection: {
    display: 'flex',
    padding: '20px 10px',
    alignItems: 'center',
  },
  uploadedAction: {
    display: 'flex',
    marginLeft: 'auto',
    padding: '0 10px',
  },
  btnPrimary: {
    display: 'block',
    fontSize: '14px',
    color: theme.textColorDark,
    cursor: 'pointer',
    padding: '0 5px',
    background: 'none',
    border: 'none',
    outline: 'none',
    textDecoration: 'none',
  },
  btnDanger: {
    display: 'block',
    fontSize: '14px',
    color: '#d32f2f',
    cursor: 'pointer',
    padding: '0 5px',
    background: 'none',
    border: 'none',
    outline: 'none',
    textDecoration: 'none',
  },
  reportedTable: {
    '& tbody tr:hover $showOnHover': {
      opacity: '1',
    },
    '& th, td': {
      padding: '4px 15px',
    },
    '& th:first-child, td:first-child': {
      maxWidth: '200px',
    },
    '& th:nth-child(2), td:nth-child(2)': {
      overflow: 'hidden',
      whiteSpace: 'nowrap',
      textOverflow: 'ellipsis',
      maxWidth: '200px',
    },
  },
  suggestionTable: {
    '& tbody tr:hover $showOnHover': {
      opacity: '1',
    },
    '& th:nth-child(3), td:nth-child(3)': {
      maxWidth: '70px',
      width: 'auto',
    },
  },
  reportedItem: {
    display: 'block',
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
  },
  sectionFooter: {
    borderTop: '1px solid #e7e7e7',
    padding: '20px',

    '& p': {
      margin: 0,
    },
    '& p + p': {
      marginTop: '20px',
    },
  },
  showOnHover: {
    display: 'flex',
    opacity: '0',
    textAlign: 'right',
  },
  tagFormContent: {
    display: 'flex',
  },
  tagForm: {
    flex: 1,
    width: '50%',
  },
  categoriesList: {
    borderBottom: `1px solid ${theme.mediumGrey}`,
  },
  category: {
    display: 'block',
    width: '100%',
    textAlign: 'left',
    border: 'none',
    background: 'transparent',
    fontSize: theme.fontSizeNormal,
    fontFamily: theme.primaryFont,
    padding: '10px 20px',
    cursor: 'pointer',

    '&:hover, &$active': {
      background: theme.lightYellow,
    },
  },
  active: {},
  formGroup: {
    display: 'flex',
    alignItems: 'center',
    padding: '8px 0 8px 15px',

    '& label': {
      minWidth: '120px',
    },

    '& input': {
      flex: '1',
      lineHeight: 'normal',
      padding: '8px 6px',
      border: 'none',
      borderBottom: `1px solid ${theme.lightGrey}`,
      fontSize: theme.fontSizeNormal,
    },
  },
  verticalFormGroup: {
    display: 'block',
    padding: '8px 15px',
  },
  formHeading: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  descriptionInput: {
    marginTop: '8px',
    padding: '12px',
    width: 'calc(100% - 24px)',
    borderColor: theme.lightGrey,
    resize: 'none',
    fontSize: theme.fontSizeNormal,
  },
  tagsPicker: {
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
    width: '50%',
    borderLeft: `1px solid ${theme.mediumGrey}`,
  },
  tagQueryInput: {
    position: 'relative',
    borderBottom: `1px solid ${theme.mediumGrey}`,

    '& input': {
      border: 'none',
      padding: '15px 54px 15px 15px',
      width: 'calc(100% - 69px)',
      height: '24px',
      fontSize: theme.fontSizeNormal,
    },

    '& svg': {
      position: 'absolute',
      top: '15px',
      right: '15px',
      height: '24px',
      cursor: 'pointer',
    },
  },
  tagsList: {
    flex: 1,
    overflow: 'auto',
    padding: '6px 0',
    maxHeight: '400px',
  },
  tagItem: {
    display: 'flex',
    justifyContent: 'space-between',
    padding: '6px 12px',

    '&:hover': {
      background: theme.lightYellow,

      '& $tagAction': {
        display: 'flex',
      },
    },
  },
  tagName: {
    display: 'block',
    maxWidth: 'calc(100% - 120px)',
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
  },
  tagAction: {
    display: 'none',
  },
  imageUpload: {
    display: 'flex',
    alignItems: 'center',
    maxWidth: 'calc(100% - 120px)',

    '& label': {
      width: '60px',
    },

    '& svg': {
      width: '15px',
    },
  },
  imageName: {
    display: 'block',
    maxWidth: 'calc(100% - 100px)',
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
  },
  confirmMessage: {
    fontFamily: theme.primaryFont,
    fontSize: '14px',
    lineHeight: 1.5,
  },
  dropdown: {
    width: '100%',
  },
  ddToggle: {
    width: '90%',
    justifyContent: 'space-between',
    borderBottom: `1px solid ${theme.mediumGrey}`,
    padding: '6px 0',
  },
})
