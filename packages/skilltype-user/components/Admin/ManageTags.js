import React from 'react'
import injectSheet from 'react-jss'
import { withServiceContext } from '@skilltype/services/components/ServiceProvider'
import { withNotifyContext } from '@skilltype/ui/components/Notify/NotifyProvider'
import RaisedSection from '@skilltype/ui/components/Section/RaisedSection'
import Table from '@skilltype/ui/components/Table/Table'
import Modal from '@skilltype/ui/components/Modal/Modal'
import TagsForm from './TagsForm'
import styles from './styles'

class ManageTags extends React.Component {
  state = {
    categories: [],
    suggestions: [],
    suggestionSelected: null,
  }

  componentDidMount = () => {
    const { serviceContext, notifyError, notifyClose } = this.props

    serviceContext.admin
      .getCategories()
      .then(categories => {
        this.setState({ categories })
      })
      .catch(err => {
        console.error(err)
        notifyError('Failed to get categories.')
        notifyClose(2000)
      })
    this.fetchData()
  }

  fetchData = () => {
    this.props.serviceContext.admin
      .getTagSuggestions()
      .then(suggestions => this.setState({ suggestions }))
  }

  formatData = data =>
    data.sort((a, b) => (a.name < b.name ? -1 : 1)).map(item => {
      const category = this.state.categories.filter(
        c => c.id === item.categoryId
      )[0]
      return [item.name, category ? category.name : '', item.count, item.id]
    })

  convertToTag = id => {
    const { serviceContext, notify, notifyError, notifyClose } = this.props

    serviceContext.admin
      .convertSuggestionToTag(id)
      .then(() => {
        notify('Suggestion has been converted to the tag.')
        notifyClose(2000)
        this.fetchData()
      })
      .catch(() => {
        notifyError('Error converting suggestion to tag.')
        notifyClose(2000)
      })
  }

  discardTagSuggestion = () => {
    const { suggestionSelected } = this.state
    const { serviceContext, notify, notifyError, notifyClose } = this.props

    serviceContext.admin
      .discardTagSuggestion(suggestionSelected)
      .then(() => {
        this.closeModal()
        notify('Suggestion has been converted to the tag.')
        notifyClose(2000)
        this.fetchData()
      })
      .catch(() => {
        this.closeModal()
        notifyError('Error converting suggestion to tag.')
        notifyClose(2000)
      })
  }

  closeModal = () => {
    this.setState({
      suggestionSelected: null,
    })
  }

  render() {
    const { suggestions, categories, suggestionSelected } = this.state
    const { classes } = this.props

    return (
      <React.Fragment>
        <RaisedSection title="Manage Tags" style={{ width: '774px' }}>
          <TagsForm categories={categories} />
        </RaisedSection>
        <RaisedSection title="Tag Suggestions" style={{ width: '774px' }}>
          <div className={classes.suggestionTable}>
            <Table
              data={this.formatData(suggestions)}
              onUpdate={this.onUpdate}
              columns={[
                { name: 'Tag' },
                { name: 'Category' },
                { name: 'Votes' },
                {
                  name: '',
                  options: {
                    customBodyRender: value => (
                      <div className={classes.showOnHover}>
                        <button
                          className={classes.btnPrimary}
                          onClick={() => this.convertToTag(value)}
                        >
                          Convert
                        </button>
                        <span>·</span>
                        <button
                          className={classes.btnDanger}
                          onClick={() =>
                            this.setState({ suggestionSelected: value })
                          }
                        >
                          Discard
                        </button>
                      </div>
                    ),
                  },
                },
              ]}
            />
          </div>
        </RaisedSection>
        {suggestionSelected && (
          <Modal
            title="Discard Tag Suggestion"
            prompt
            showOkButton
            okButtonLabel="Discard"
            showCloseButton
            showCancelButton
            onOk={this.discardTagSuggestion}
            onDismiss={this.closeModal}
            onCancel={this.closeModal}
          >
            <div className={classes.confirmMessage}>
              Are you sure you would like to discard the suggestion?
            </div>
          </Modal>
        )}
      </React.Fragment>
    )
  }
}

export default injectSheet(styles)(
  withServiceContext(withNotifyContext(ManageTags))
)
