import React from 'react'
import injectSheet from 'react-jss'
import { profileTagGroups } from '@skilltype/data'
import Modal from '@skilltype/ui/components/Modal/Modal'
import DesktopOnly from '@skilltype/ui/components/Responsive/DesktopOnly'
import ProfileCard from '@skilltype/ui/components/ProfileCard/ProfileCard'
import PrimaryButton from '@skilltype/ui/components/Button/PrimaryButton'
import Checkbox from '@skilltype/ui/components/Checkbox/Checkbox'
import AddResource from '@skilltype/ui/modules/Library/AddResource'
import TagResource from '@skilltype/ui/modules/Library/TagResource'
import { prefixSearch } from '@skilltype/services/lib/suggestions'
import NavMenu from './NavMenu'
import { withUserContext } from '../User/UserProvider'
import styles from './styles'

const {
  suggestionsByType: { skills },
} = profileTagGroups

class AddResourceRouter extends React.Component {
  state = {
    values: {},
  }
  render() {
    return (
      <AddResource
        onChange={({ values }) => {
          this.setState({ values })
        }}
        values={this.state.values}
      />
    )
  }
}

class TagResourceRouter extends React.Component {
  state = {
    tagList: [],
    suggestions: skills,
    query: '',
  }

  render() {
    return (
      <TagResource
        tagList={this.state.tagList}
        querySuggestions={this.state.suggestions}
        query={this.state.query}
        onChange={tagList => this.setState({ tagList })}
        onQueryChange={query =>
          this.setState({
            query,
            suggestions: prefixSearch(skills, query),
          })
        }
      />
    )
  }
}

class LeftSidebar extends React.Component {
  state = {
    showModal: false,
    step: 0,
    items: [],
  }

  componentDidMount() {
    const { feedPage, userContext } = this.props

    if (feedPage) {
      userContext.fetchFeedItemTypes(res => {
        this.setState({ items: res })
      })
    }
  }

  onOK = () => {
    const { step } = this.state

    if (step) {
      this.dismissModal()
    } else {
      this.setState({ step: 1 })
    }
  }

  onCancel = () => {
    const { step } = this.state

    if (step) {
      this.setState({ step: 0 })
    } else {
      this.dismissModal()
    }
  }

  onCheckItem = (item, checked) => {
    const { userContext } = this.props
    const selected = [...userContext.itemTypesSelected]

    if (checked) {
      selected.push(item.code)
    } else {
      selected.splice(selected.indexOf(item.code), 1)
    }

    userContext.selectItemType(selected)
  }

  showModal = () => {
    this.setState({ showModal: true, step: 0 })
  }

  dismissModal = () => {
    this.setState({ showModal: false })
  }

  render() {
    const { classes, userData, feedPage } = this.props
    const { items, showModal, step } = this.state
    const location =
      userData.city && userData.state
        ? `${userData.city}, ${userData.state}`
        : ''
    const cardData = {
      profileTheme: userData.cardColor || 'fog',
      name: `${userData.firstName} ${userData.lastName}`,
      tagline: userData.tagline || '',
      location,
      avatarLabel:
        userData.firstName.charAt(0).toUpperCase() +
        userData.lastName.charAt(0).toUpperCase(),
      verified: false,
    }

    return (
      <DesktopOnly>
        <div className={classes.leftSidebar}>
          <div className={classes.sidebarContent}>
            <ProfileCard {...cardData} style={{ marginBottom: '1em' }} />
            <NavMenu />
            <PrimaryButton
              className={classes.addItemBtn}
              onClick={this.showModal}
            >
              Add Item
            </PrimaryButton>

            {feedPage && (
              <div className={classes.leftFilter}>
                {items.map((item, idx) => (
                  <Checkbox
                    key={idx}
                    label={item.description}
                    onChange={e => this.onCheckItem(item, e.target.value)}
                  />
                ))}
              </div>
            )}
            {showModal && (
              <Modal
                title="Add Resource"
                showOkButton
                okButtonLabel={step ? 'Add' : 'Next'}
                showCancelButton
                cancelButtonLabel={step ? 'Back' : 'Cancel'}
                showCloseButton
                lockBodyScroll
                hasEditableContent
                contentHandlesScroll={!!step}
                onOk={this.onOK}
                onCancel={this.onCancel}
                onDismiss={this.dismissModal}
                appElementId="root"
              >
                {step ? <TagResourceRouter /> : <AddResourceRouter />}
              </Modal>
            )}
          </div>
        </div>
      </DesktopOnly>
    )
  }
}

export default withUserContext(injectSheet(styles)(LeftSidebar))
