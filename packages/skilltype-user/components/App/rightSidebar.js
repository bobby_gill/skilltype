import React from 'react'
import injectSheet from 'react-jss'
import DesktopOnly from '@skilltype/ui/components/Responsive/DesktopOnly'
import styles from './styles'

const RightSidebar = ({ classes, children }) => (
  <DesktopOnly>
    <div className={classes.rightSidebar}>
      <div className={classes.sidebarContent}>{children}</div>
    </div>
  </DesktopOnly>
)

export default injectSheet(styles)(RightSidebar)
