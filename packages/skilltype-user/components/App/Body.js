import React from 'react'
import injectSheet from 'react-jss'
import Page from '@skilltype/ui/components/Viewport/Page'
import LeftSidebar from './leftSidebar'
import styles from './styles'

const Body = ({ classes, userData, feedPage, children }) => (
  <Page>
    <LeftSidebar userData={userData} feedPage={feedPage} />
    <div className={classes.mainContent}>{children}</div>
  </Page>
)

export default injectSheet(styles)(Body)
