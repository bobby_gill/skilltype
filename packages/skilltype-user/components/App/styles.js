import { macros, theme as defaultTheme } from '@skilltype/ui'

export default (theme = defaultTheme) => ({
  leftSidebar: {
    position: 'fixed',
    maxHeight: 'calc(100vh - 100px)',
    overflow: 'auto',
    scrollbarWidth: 'none',
    '-ms-overflow-style': 'none',

    '&::-webkit-scrollbar': {
      width: '0',
      height: '0',
    },
  },
  sidebarContent: {
    display: 'flex',
    flexDirection: 'column',
  },
  mainContent: {
    display: 'flex',
    flexDirection: 'column',

    ...macros.desktop(
      {
        marginLeft: '225px',
      },
      theme
    ),
  },
  rightSidebar: {
    position: 'fixed',
    right: '15px',
    width: '250px',
    maxHeight: 'calc(100vh - 100px)',
    overflow: 'auto',
    scrollbarWidth: 'none',
    '-ms-overflow-style': 'none',

    '&::-webkit-scrollbar': {
      width: '0',
      height: '0',
    },
  },
  addItemBtn: {
    margin: '15px 0',
  },
  leftFilter: {
    padding: '0 10px',

    '& > label': {
      display: 'flex',

      '& span': {
        fontSize: '14px',
      },
    },
  },
})
